variables:
  CARGO_HOME: "$CI_PROJECT_DIR/.cargo"

.build_template: &build_definition
  script:
    - "rustc --version && cargo --version"
    - cargo build -p "$PKGID" --verbose

.build_oldest_template:
  <<: *build_definition
  before_script:
    - apk add musl-dev
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "development"'

.build_latest_template:
  <<: *build_definition
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "development"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

.test_run_template:
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev
  script:
    - "rustc --version && cargo --version"
    - cargo test -p "$PKGID" --verbose
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "development"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

.cover_template:
  # Tarpaulin does not work in Alpine image.
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev curl grep coreutils wget tar gzip
    - >
      curl -s https://api.github.com/repos/xd009642/tarpaulin/releases/latest
      | grep "https.*cargo-tarpaulin-x86_64-unknown-linux-musl.tar.gz"
      | cut -d '"' -f 4
      | wget -q -i - -O -
      | tar -xzp
    - mv cargo-tarpaulin /usr/local/cargo/bin
  script:
    - cd "$FOLDER"
    - "rustc --version && cargo --version"
    - cargo tarpaulin -p "$PKGID" -v --ignore-tests --exclude-files cargo -o Html
  coverage: '/^\d+.\d+% coverage/'
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
    policy: pull
  artifacts:
    paths:
      - ./tarpaulin-report.html
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "development"'

.examples_run_template:
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev make
  script:
    - cd "$FOLDER"
    - "rustc --version && cargo --version"
    - make all_examples
  needs: ["test:run"]
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
    policy: pull
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "development"'

.package_build_template:
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev
  script:
    - "rustc --version && cargo --version"
    - cargo package -p "$PKGID" --list --verbose
    - cargo package -p "$PKGID" --verbose
  # Do not fetch artifacts from previous stages
  dependencies: []
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
    policy: pull
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
      allow_failure: true
    - if: '$CI_COMMIT_BRANCH == "development"'
      when: manual
      allow_failure: true

.publish_send_template:
  image: 'rust:alpine'
  before_script:
    - apk add musl-dev
  script:
    - "rustc --version && cargo --version"
    - cargo publish -p "$PKGID" --verbose
  # Do not fetch artifacts from previous stages
  dependencies: []
  cache:
    key: "$CI_PIPELINE_IID"
    paths:
      - target/
      - .cargo/registry/index
      - .cargo/registry/cache
    policy: pull
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
      allow_failure: true
