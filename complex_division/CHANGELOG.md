# Changelog

## [1.0.1] - 2022-07-17
## Added
- None
## Changed
- Oldest supported rustc version has been increased to 1.56
### API Changes
- None
## Fixed
- None


## [1.0.0] - 2022-04-04
## Added
- Splitted package from `automatica`
## Changed
- None
### API Changes
- None
## Fixed
- None
