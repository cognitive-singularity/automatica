<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="pandoc">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <title>Polynomen</title>
    <link rel="stylesheet" href="./style.css">
  </head>
  <body>
    <header class="dark-background">
      <h1 class="title">Polynomen</h1>
      <div id="license">
        This work is licensed under
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>
      </div>
    </header>
    <div class="container">
      <div class="sidebar dark-background">
        <div id="table-of-contents">
          <h2>Table of contents</h2>
          <nav id="toc">
            <ul>
            <li><a href="#polynomial-library">Polynomial Library</a></li>
            <li><a href="#introduction">Introduction</a>
            <ul>
            <li><a href="#purpose">Purpose</a></li>
            <li><a href="#scope">Scope</a></li>
            <li><a href="#definitions-acronyms-and-abbreviations">Definitions, acronyms and abbreviations</a></li>
            <li><a href="#overview">Overview</a></li>
            </ul></li>
            <li><a href="#overall-description">Overall description</a>
            <ul>
            <li><a href="#product-perspective">Product perspective</a></li>
            <li><a href="#product-functions">Product functions</a></li>
            <li><a href="#constraints">Constraints</a></li>
            </ul></li>
            <li><a href="#specific-requirements">Specific requirements</a>
            <ul>
            <li><a href="#external-interfaces">External interfaces</a></li>
            <li><a href="#functional-requirements">Functional requirements</a></li>
            </ul></li>
            <li><a href="#test-plan">Test Plan</a>
            <ul>
            <li><a href="#black-box-testing">Black box testing</a></li>
            <li><a href="#white-box-testing">White box testing</a></li>
            <li><a href="#documentation-tests">Documentation tests</a></li>
            <li><a href="#test-coverage">Test coverage</a></li>
            </ul></li>
            <li><a href="#continuous-integration">Continuous integration</a>
            <ul>
            <li><a href="#description">Description</a></li>
            <li><a href="#structure">Structure</a></li>
            </ul></li>
            <li><a href="#references">References</a></li>
            </ul>
          </nav>
        </div>
        <div id="external-links">
          <h2>External links</h2>
          <ul>
            <li><a href="https://gitlab.com/daingun/automatica">Source code repository</a></li>
            <li><a href="https://crates.io/crates/polynomen">Crate registry</a></li>
            <li><a href="https://docs.rs/polynomen">API documentation</a></li>
          </ul>
        </div>
        <div id="version">
          <h2>Version</h2>
          1.1.0
        </div>
      </div>
      <div class="content">
        <h1 id="polynomial-library">Polynomial Library</h1>
        <p>This library contains also a module for the creation and manipulation of polynomials, for which all the arithmetical operations, the derivative, the integration, the evaluation with real and complex numbers are defined, real and complex roots can be calculated.</p>
        <h1 id="introduction">Introduction</h1>
        <h2 id="purpose">Purpose</h2>
        <p>This SRS (Software Requirements Specification) has the purpose of supplying the reference for the high level description of the software, of its design and its verification.</p>
        <p>This document is addressed to those that intend to use this library in their software or those that intend to contribute to the development of the library.</p>
        <h2 id="scope">Scope</h2>
        <p>The name of the library is <code>polynomen</code>. The chosen programming language for the development is <code>Rust</code> [8].</p>
        <p>The version control system is <code>git</code> [9] and the source code is hosted on <code>Gitlab</code> at the following link:</p>
        <p><a href="https://gitlab.com/daingun/automatica" class="uri">https://gitlab.com/daingun/automatica</a></p>
        <p>The access to the public registry crates.io [10] is available at the following link:</p>
        <p><a href="https://crates.io/crates/polynomen" class="uri">https://crates.io/crates/polynomen</a></p>
        <p>The library supplies an infrastructure that contains calculation methods for polynomials. This software does not supply an interface with the final user.</p>
        <h2 id="definitions-acronyms-and-abbreviations">Definitions, acronyms and abbreviations</h2>
        <p><em>Polynomial</em>: algebraic sum of monomials made by a coefficient and a literal part;</p>
        <p><em>by_value</em>: the method parameter is passed as value;</p>
        <p><em>by_ref</em>: the method parameter is passed as reference;</p>
        <p><em>in_place</em>: the method parameter is passed as mutable reference;</p>
        <p><em>FR</em>: Functional requirement;</p>
        <p><em>TC</em>: Test case.</p>
        <h2 id="overview">Overview</h2>
        <p>This SRS is subdivided in a general description of the library that contains the interfaces and the requirements for the software, a description of specific and functional requirements of the library that describe what it is able to execute, a description of the test plan both black box and white box type, a description of the continuous integration process.</p>
        <h1 id="overall-description">Overall description</h1>
        <h2 id="product-perspective">Product perspective</h2>
        <h3 id="system-interfaces">System interfaces</h3>
        <p>The library can be used on platforms on which the support for compilation exists. For a reference to the available platforms it is possible to refer to the official list [11].</p>
        <h3 id="user-interfaces">User interfaces</h3>
        <p>This library is not designed to have a direct interaction with the user, therefore there are not user interfaces.</p>
        <h3 id="hardware-interfaces">Hardware Interfaces</h3>
        <p>This library does not directly interact with the hardware, it uses the function made available by the <code>Rust</code> standard library and the operating system in use.</p>
        <h3 id="software-interfaces">Software interfaces</h3>
        <p>The library needs the <code>rustc</code> compiler for the compilation with a version equal of greater than 1.56.</p>
        <p>The list of the needed dependencies for the compilations of the library is the following:</p>
        <ul>
        <li>ndarray
        <ul>
        <li>multidimensional vectors, allows non numerical elements</li>
        <li>source: <a href="https://crates.io/crates/ndarray" class="uri">https://crates.io/crates/ndarray</a></li>
        <li>version: 0.15</li>
        </ul></li>
        <li>complex-division
        <ul>
        <li>algorithm for complex numbers division</li>
        <li>source: <a href="https://crates.io/crates/complex-division" class="uri">https://crates.io/crates/complex-division</a></li>
        <li>version: 1.0</li>
        </ul></li>
        <li>zip-fill
        <ul>
        <li>iterator extensions</li>
        <li>source: <a href="https://crates.io/crates/zip-fill" class="uri">https://crates.io/crates/zip-fill</a></li>
        <li>version: 1.0</li>
        </ul></li>
        </ul>
        <p>The following dependencies are necessary for the development phase:</p>
        <ul>
        <li>approx
        <ul>
        <li>approximate equality for floating point numbers</li>
        <li>source: <a href="https://crates.io/crates/approx" class="uri">https://crates.io/crates/approx</a></li>
        <li>version: 0.4</li>
        </ul></li>
        <li>proptest
        <ul>
        <li>randomized tests</li>
        <li>source: <a href="https://crates.io/crates/proptest" class="uri">https://crates.io/crates/proptest</a></li>
        <li>version: 0.10</li>
        </ul></li>
        </ul>
        <h3 id="communications-interfaces">Communications interfaces</h3>
        <p>This library does not have network interfaces.</p>
        <h3 id="memory">Memory</h3>
        <p>There are not memory limits for the use of this library.</p>
        <h2 id="product-functions">Product functions</h2>
        <p>The library allows the calculation of polynomials, it includes the arithmetical operations and the methods for the calculation of the roots of the polynomial.</p>
        <h2 id="constraints">Constraints</h2>
        <p>There are non constraints in the use of this library. Regarding the development, the correctness must be the first aim.</p>
        <h1 id="specific-requirements">Specific requirements</h1>
        <h2 id="external-interfaces">External interfaces</h2>
        <h3 id="data-structures">Data structures</h3>
        <p>This library must present as interface structures of the <code>Rust</code> standard library, or structure created by this library.</p>
        <p>Using as interface as structure defined in a dependency exposes the internal implementation ad forces the use of that dependency.</p>
        <p>When this library must expose traits from its dependencies its shall re-export those crates.</p>
        <h2 id="functional-requirements">Functional requirements</h2>
        <h3 id="polynomial-creation">Polynomial creation</h3>
        <p><em>FR.1</em> The user shall be able to create polynomials with real coefficients, supplying the coefficients of the roots, both as list or as iterator.</p>
        <p><em>FR.2</em> In particular the coefficients shall be supplied from the lowest to the highest degree monomial. It is necessary to put zeros where the monomial is null.</p>
        <p><em>FR.3</em> Both the null polynomial and the unity polynomial can be created.</p>
        <h3 id="indexing">Indexing</h3>
        <p><em>FR.4</em> The coefficients can be indexed, the index of the monomial is equal to its degree. The indexed coefficient shall be modifiable.</p>
        <h3 id="properties">Properties</h3>
        <p>It is possible to operate on polynomial properties:</p>
        <p><em>FR.5</em> Degree calculation (it is undefined for null polynomial) (by ref);</p>
        <p><em>FR.6</em> Extension of the polynomial with zeros in the highest degree monomials (in place);</p>
        <p><em>FR.7</em> The transformation into a monic polynomial and the return of the leading coefficient (in place, by ref);</p>
        <p><em>FR.8</em> Evaluation of a polynomial both with real and complex numbers, as well as other polynomials, using Horner’s method (by value, by ref);</p>
        <p><em>FR.9</em> Rounding towards zero of the coefficients given an absolute tolerance (by ref, in place).</p>
        <h3 id="roots">Roots</h3>
        <p><em>FR.10</em> It is possible to calculate the roots of a polynomial. When asking for the complex roots, both the eigenvalues and iterative [1][2][3][4][6][7] methods are available. In the case the user needs real roots, a result is supplied (with eigenvalues method) only when all roots are real.</p>
        <h3 id="arithmetical-operations-and-infinitesimal-calculus">Arithmetical operations and infinitesimal calculus</h3>
        <p><em>FR.11</em> On polynomial it is possible to perform arithmetical operations, both between polynomials and scalars, and operations of infinitesimal calculus:</p>
        <ul>
        <li>negation of the polynomial (by value, by ref)</li>
        <li>addition, subtraction, division and division reminder [5] between polynomials (by value, by ref)</li>
        <li>multiplication between polynomials both with the convolution method and the fast Fourier transform method [6] (by value, by ref)</li>
        <li>addition, subtraction, multiplication, and division with a scalar (by value, by ref)</li>
        <li>calculation of the derivative and the integral of the polynomial (by ref)</li>
        <li>evaluation of polynomial ratios avoiding overflows (by ref)</li>
        <li>exponentiation with a positive exponent (by ref)</li>
        </ul>
        <h3 id="formatting">Formatting</h3>
        <p><em>FR.12</em> It is available a standard formatting for the output of the polynomial as string.</p>
        <h3 id="polynomial-norms">Polynomial norms</h3>
        <p><em>FR.13</em> The calculation of the following norms of the polynomial shall be available:</p>
        <ul>
        <li>l1-norm: the sum of the absolute values of the coefficients</li>
        <li>l2-norm: the square root of the sum of the squares of the coefficients</li>
        <li>l∞-norm: the maximum of the absolute values of the coefficients</li>
        </ul>
        <h3 id="polynomial-greatest-common-divisor">Polynomial greatest common divisor</h3>
        <p><em>FR.14</em> The calculation of the greater common divisor. The result is not required to be in monic form.</p>
        <h1 id="test-plan">Test Plan</h1>
        <h2 id="black-box-testing">Black box testing</h2>
        <p>Black box tests verify the properties of the mathematical entities defined in the library.</p>
        <p>Tests have defined the path in the test files, the inputs and the expected outputs.</p>
        <p><em>TC.1</em> A polynomial multiplied by one returns the same polynomial.</p>
        <pre><code>tests/polynomial/multiplicative_unity</code></pre>
        <ul>
        <li>input: p1 (arbitrary polynomial, null polynomial), p2 (unity polynomial, scalar 1)</li>
        <li>output: p1</li>
        </ul>
        <p><em>TC.2</em> A polynomial multiplied by zero returns zero.</p>
        <pre><code>tests/polynomial/multiplicative_null</code></pre>
        <ul>
        <li>input: p1 (arbitrary polynomial, null polynomial), p2 (null polynomial, scalar 0)</li>
        <li>output: null polynomial</li>
        </ul>
        <p><em>TC.3</em> If a non-null polynomial is multiplied by another polynomial and divided by the same, the result is equal to the first polynomial.</p>
        <pre><code>tests/polynomial/multiplicative_inverse</code></pre>
        <ul>
        <li>input: p1 (arbitrary polynomial), p2 (arbitrary polynomial)</li>
        <li>output: p1</li>
        </ul>
        <p><em>TC.4</em> A polynomial plus or minus zero is equal the the polynomial itself.</p>
        <pre><code>tests/polynomial/additive_invariant</code></pre>
        <ul>
        <li>input: p1 (arbitrary polynomial), p2 (null polynomial, scalar 0)</li>
        <li>output: p1</li>
        </ul>
        <p><em>TC.5</em> If to a polynomial is added and subtracted another polynomial, the result is equal to the first polynomial.</p>
        <pre><code>tests/polynomial/additive_inverse</code></pre>
        <ul>
        <li>input: p1 (arbitrary polynomial), p2 (arbitrary polynomial)</li>
        <li>output: p1</li>
        </ul>
        <p><em>TC.6</em> The number of roots of a polynomial is equal to its degree.</p>
        <pre><code>tests/polynomial/roots_degree</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mn>1</mn><mo>,</mo><mn>1</mn><mo>+</mo><mn>2</mn><mi>x</mi><mo>,</mo><mn>1</mn><mo>+</mo><mn>2</mn><mi>x</mi><mo>+</mo><mn>3</mn><msup><mi>x</mi><mn>2</mn></msup></mrow><annotation encoding="application/x-tex">1, 1+2x, 1+2x+3x^{2}</annotation></semantics></math></li>
        <li>output: Some(0), Some(1), Some(2)</li>
        </ul>
        <p><em>TC.7</em> A null polynomial does not have a defined degree.</p>
        <pre><code>tests/polynomial/no_degree</code></pre>
        <ul>
        <li>input: null polynomial</li>
        <li>output: None</li>
        </ul>
        <p><em>TC.8</em> The derivative of a polynomial has one less degree than the starting polynomial.</p>
        <pre><code>tests/polynomial/derivation</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mn>2</mn><mi>x</mi><mo>+</mo><mn>3</mn><msup><mi>x</mi><mn>2</mn></msup><mo>,</mo><mn>2</mn><mo>+</mo><mn>6</mn><mi>x</mi><mo>,</mo><mn>6</mn></mrow><annotation encoding="application/x-tex">2x+3x^{2}, 2+6x, 6</annotation></semantics></math></li>
        <li>output: Some(1), Some(0), None</li>
        </ul>
        <p><em>TC.9</em> The integral of a polynomial has one more degree than the starting polynomial.</p>
        <pre><code>tests/polynomial/integration</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mn>0</mn><mo>,</mo><mo>−</mo><mn>1</mn><mo>+</mo><mi>x</mi><mo>,</mo><mn>2</mn><mo>−</mo><mi>x</mi><mo>+</mo><mfrac><msup><mi>x</mi><mn>2</mn></msup><mn>2</mn></mfrac></mrow><annotation encoding="application/x-tex">0, -1+x, 2-x+\frac{x^{2}}{2}</annotation></semantics></math></li>
        <li>output: Some(0), Some(1), Some(2)</li>
        </ul>
        <p><em>TC.10</em> The stationary points of a polynomial function are the roots of its derivative.</p>
        <pre><code>tests/polynomial/maximum_minimum</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><mo stretchy="true" form="prefix">(</mo><mn>1</mn><mo>+</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mi>x</mi><mrow><mo stretchy="true" form="prefix">(</mo><mo>−</mo><mn>1</mn><mo>+</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow></mrow><annotation encoding="application/x-tex">\left(1+x\right)x\left(-1+x\right)</annotation></semantics></math></li>
        <li>output: stationary points in <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi>x</mi><mo>=</mo><mo>±</mo><mn>0.57735</mn></mrow><annotation encoding="application/x-tex">x=\pm 0.57735</annotation></semantics></math></li>
        </ul>
        <p><em>TC.11</em> The iterative root finding method must be stable for nearly multiple zeros ([13] page 6).</p>
        <pre><code>tests/polynomial/nearly_multiple_zeros</code></pre>
        <ul>
        <li>input: P4, P5, P6, P8</li>
        <li>output: roots must all be real, from root it is possible to recover the original polynomial with given error or roots must be within given error.</li>
        </ul>
        <p><em>TC.12</em> The iterative root finding method must be stable for equimodular zeros ([13] page 7).</p>
        <pre><code>tests/polynomial/equimodular_zeros</code></pre>
        <ul>
        <li>input: P9</li>
        <li>output: there shall be 20 roots, all root must have a modulus of either 100 or 0.01</li>
        </ul>
        <p><em>TC.13</em> Test defects in the root finding algorithm ([13] page 8).</p>
        <pre><code>tests/polynomial/defects_in_algorithm</code></pre>
        <ul>
        <li>input: P10</li>
        <li>output: all roots must be equal to the one use to build the polynomials</li>
        </ul>
        <p><em>TC.14</em> Test defects in the root finding algorithm by calculating roots on a unit circle and a circle of radius 0.9 ([13] page 8).</p>
        <pre><code>tests/polynomial/defects_on_circle</code></pre>
        <ul>
        <li>input: P11</li>
        <li>output: all roots must have modulus of either 1.0 or 0.9</li>
        </ul>
        <p><em>TC.15</em> Test the consistency of root finding algorithm by calculating the roots oof the Wilkinson’s polynomial, which are the natural numbers from 1 to 20 included.</p>
        <pre><code>tests/polynomial/roots_consistency</code></pre>
        <ul>
        <li>input: Wilkinson’s polynomial</li>
        <li>output: all roots must be within a given relative errors</li>
        </ul>
        <p><em>TC.16</em> Test arithmetic operations.</p>
        <pre><code>tests/polynomial/arithmetics</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>p</mi><mn>1</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>1</mn><mo>+</mo><mi>x</mi><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup><mo>,</mo><msub><mi>p</mi><mn>2</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mo>−</mo><mn>1</mn><mo>−</mo><mi>x</mi><mo>−</mo><msup><mi>x</mi><mn>2</mn></msup><mo>,</mo><msub><mi>p</mi><mn>3</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>1</mn><mo>+</mo><mi>x</mi><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><msup><mi>x</mi><mn>3</mn></msup><mo>+</mo><msup><mi>x</mi><mn>4</mn></msup><mo>,</mo><msub><mi>p</mi><mn>4</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mo>−</mo><mn>1</mn><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup></mrow><annotation encoding="application/x-tex">p_1\left(x\right)=1+x+x^2, p_2\left(x\right)=-1-x-x^2, p_3\left(x\right)=1+x+x^2+x^3+x^4, p_4\left(x\right)=-1+x^2</annotation></semantics></math></li>
        <li>output: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>p</mi><mn>1</mn></msub><mo>+</mo><msub><mi>p</mi><mn>2</mn></msub><mo>=</mo><mn>0</mn><mo>,</mo><mi>q</mi><mo>=</mo><msub><mi>p</mi><mn>3</mn></msub><mi>/</mi><msub><mi>p</mi><mn>4</mn></msub><mo>=</mo><mn>2</mn><mo>+</mo><mi>x</mi><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup><mo>,</mo><mi>r</mi><mo>=</mo><msub><mi>p</mi><mn>3</mn></msub><mi>%</mi><msub><mi>p</mi><mn>4</mn></msub><mo>=</mo><mn>3</mn><mo>+</mo><mn>2</mn><mi>x</mi><mo>,</mo><msub><mi>p</mi><mn>4</mn></msub><mo>⋅</mo><mi>q</mi><mo>+</mo><mi>r</mi><mo>=</mo><msub><mi>p</mi><mn>3</mn></msub></mrow><annotation encoding="application/x-tex">p_1 + p_2 = 0, q=p_3 / p_4 = 2+x+x^2, r=p_3 \% p_4 = 3+2x, p_4 \cdot q + r = p_3</annotation></semantics></math></li>
        </ul>
        <p><em>TC.17</em> Test the generation of Chebyshev polynomial of first kind.</p>
        <pre><code>tests/polynomial/chebyshev_first_kind</code></pre>
        <ul>
        <li>input: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>0</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>1</mn><mo>,</mo><msub><mi>T</mi><mn>1</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mi>x</mi></mrow><annotation encoding="application/x-tex">T_0\left(x\right)=1, T_1\left(x\right)=x</annotation></semantics></math></li>
        <li>output: <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>2</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>2</mn><msup><mi>x</mi><mn>2</mn></msup><mo>−</mo><mn>1</mn></mrow><annotation encoding="application/x-tex">T_2\left(x\right)=2x^2-1</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>3</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>4</mn><msup><mi>x</mi><mn>3</mn></msup><mo>−</mo><mn>3</mn><mi>x</mi></mrow><annotation encoding="application/x-tex">T_3\left(x\right)=4x^3-3x</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>4</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>8</mn><msup><mi>x</mi><mn>4</mn></msup><mo>−</mo><mn>8</mn><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn></mrow><annotation encoding="application/x-tex">T_4\left(x\right)=8x^4-8x^2+1</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>5</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>16</mn><msup><mi>x</mi><mn>5</mn></msup><mo>−</mo><mn>20</mn><msup><mi>x</mi><mn>3</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi></mrow><annotation encoding="application/x-tex">T_5\left(x\right)=16x^5-20x^3+5x</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>6</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>32</mn><msup><mi>x</mi><mn>6</mn></msup><mo>−</mo><mn>48</mn><msup><mi>x</mi><mn>4</mn></msup><mo>+</mo><mn>18</mn><msup><mi>x</mi><mn>2</mn></msup><mo>−</mo><mn>1</mn></mrow><annotation encoding="application/x-tex">T_6\left(x\right)=32x^6-48x^4+18x^2-1</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>7</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>64</mn><msup><mi>x</mi><mn>7</mn></msup><mo>−</mo><mn>112</mn><msup><mi>x</mi><mn>5</mn></msup><mo>+</mo><mn>56</mn><msup><mi>x</mi><mn>3</mn></msup><mo>−</mo><mn>7</mn><mi>x</mi></mrow><annotation encoding="application/x-tex">T_7\left(x\right)=64x^7-112x^5+56x^3-7x</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>8</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>128</mn><msup><mi>x</mi><mn>8</mn></msup><mo>−</mo><mn>256</mn><msup><mi>x</mi><mn>6</mn></msup><mo>+</mo><mn>160</mn><msup><mi>x</mi><mn>4</mn></msup><mo>−</mo><mn>32</mn><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn></mrow><annotation encoding="application/x-tex">T_8\left(x\right)=128x^8-256x^6+160x^4-32x^2+1</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>9</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>256</mn><msup><mi>x</mi><mn>9</mn></msup><mo>−</mo><mn>576</mn><msup><mi>x</mi><mn>7</mn></msup><mo>+</mo><mn>432</mn><msup><mi>x</mi><mn>5</mn></msup><mo>−</mo><mn>120</mn><msup><mi>x</mi><mn>3</mn></msup><mo>+</mo><mn>9</mn><mi>x</mi></mrow><annotation encoding="application/x-tex">T_9\left(x\right)=256x^9-576x^7+432x^5-120x^3+9x</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>10</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>512</mn><msup><mi>x</mi><mn>10</mn></msup><mo>−</mo><mn>1280</mn><msup><mi>x</mi><mn>8</mn></msup><mo>+</mo><mn>1120</mn><msup><mi>x</mi><mn>6</mn></msup><mo>−</mo><mn>400</mn><msup><mi>x</mi><mn>4</mn></msup><mo>+</mo><mn>50</mn><msup><mi>x</mi><mn>2</mn></msup><mo>−</mo><mn>1</mn></mrow><annotation encoding="application/x-tex">T_10\left(x\right)=512x^10-1280x^8+1120x^6-400x^4+50x^2-1</annotation></semantics></math><br />
        <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>T</mi><mn>11</mn></msub><mrow><mo stretchy="true" form="prefix">(</mo><mi>x</mi><mo stretchy="true" form="postfix">)</mo></mrow><mo>=</mo><mn>1024</mn><msup><mi>x</mi><mn>11</mn></msup><mo>−</mo><mn>2816</mn><msup><mi>x</mi><mn>9</mn></msup><mo>+</mo><mn>2816</mn><msup><mi>x</mi><mn>7</mn></msup><mo>−</mo><mn>1232</mn><msup><mi>x</mi><mn>5</mn></msup><mo>+</mo><mn>220</mn><msup><mi>x</mi><mn>3</mn></msup><mo>−</mo><mn>11</mn><mi>x</mi></mrow><annotation encoding="application/x-tex">T_11\left(x\right)=1024x^11-2816x^9+2816x^7-1232x^5+220x^3-11x</annotation></semantics></math></li>
        </ul>
        <p><em>TC.18</em> Chebyshev polynomials roots.</p>
        <pre><code>tests/polynomial/chebyshev_roots</code></pre>
        <ul>
        <li>input: Chebyshev polynomials from <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><msub><mi>T</mi><mn>2</mn></msub><annotation encoding="application/x-tex">T_2</annotation></semantics></math> to <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><msub><mi>T</mi><mn>11</mn></msub><annotation encoding="application/x-tex">T_11</annotation></semantics></math></li>
        <li>output: The roots are the values given of the following function <math display="inline" xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi>c</mi><mi>o</mi><mi>s</mi><mrow><mo stretchy="true" form="prefix">(</mo><mfrac><mrow><mrow><mo stretchy="true" form="prefix">(</mo><mn>2</mn><mi>k</mi><mo>−</mo><mn>1</mn><mo stretchy="true" form="postfix">)</mo></mrow><mi>π</mi></mrow><mrow><mn>2</mn><mi>n</mi></mrow></mfrac><mo stretchy="true" form="postfix">)</mo></mrow></mrow><annotation encoding="application/x-tex">cos\left(\frac{\left(2k-1\right)\pi}{2n}\right)</annotation></semantics></math></li>
        </ul>
        <h2 id="white-box-testing">White box testing</h2>
        <p>Every module must contain a test submodule, whose purpose it to test all the functions present inside the module.</p>
        <p>This tests are dependent from the implementation of the method, it is not given a detailed description, but the requirements are listed and what the test shall verify. More information must be added in the source code.</p>
        <h3 id="generic-methods">Generic methods</h3>
        <p>Formatting polynomial as string.</p>
        <p>Creation from coefficient and from roots, supplied both as a list and as a iterator, creation of the null and unity polynomial.</p>
        <p>Return coefficients as a list.</p>
        <p>Calculation of the length and the degree of the polynomial.</p>
        <p>Extension with zeros of the high degree coefficients.</p>
        <p>Evaluation of the polynomial both with real and complex numbers.</p>
        <p>Indexing of polynomial coefficients.</p>
        <p>Derivative and integral of the polynomial.</p>
        <p>Transformation into a monic polynomial and return of the leading coefficient.</p>
        <p>Rounding towards zero of the coefficients.</p>
        <p>Creation of the companion matrix.</p>
        <p>Polynomial norms.</p>
        <h3 id="arithmetic">arithmetic</h3>
        <p>Arithmetic operations defined between polynomial and with real numbers.</p>
        <p>Evaluation of polynomial ratios without overflows.</p>
        <p>Multiplication through fast Fourier transformation.</p>
        <p>Greatest common divisor.</p>
        <h3 id="fft">fft</h3>
        <p>Bit permutation of a integer number.</p>
        <p>Calculation of the direct and inverse fast Fourier transformation.</p>
        <h3 id="roots-1">roots</h3>
        <p>Calculation of the real and complex roots of a polynomial of any degree, both with the method of eigenvalues and the iterative method. The null roots shall be removed from the calculation.</p>
        <h3 id="convex-hull">convex hull</h3>
        <p>Top convex hull of a set of points. Vectors turns and vectors cross product.</p>
        <h2 id="documentation-tests">Documentation tests</h2>
        <p>Every public method of the library shall have a documentation in the source code, in addition there shall be an example of the use of the method.</p>
        <p>This example will compiled and executed during the test phase, ensuring that the documentation remains aligned with the code modifications.</p>
        <h2 id="test-coverage">Test coverage</h2>
        <p>The test development shall tend to obtain the maximum line coverage.</p>
        <p>The program to determine the test coverage is <code>tarpaulin</code> [12], it is executed during the continuous integration phases.</p>
        <h1 id="continuous-integration">Continuous integration</h1>
        <h2 id="description">Description</h2>
        <p>The use of GitLab platform for the management of the source code of the library, allows the management of the continuous integration activities.</p>
        <p>The process of continuous integration allows to have the certainty that, after changes to the library, the code can compile and that all tests are executed.</p>
        <p>The pipeline is executed every time that the changes to the source code are pushed to the repository. Failures during the process are reported by email.</p>
        <h2 id="structure">Structure</h2>
        <p>The pipeline is made by the following list of stage and jobs:</p>
        <ol type="1">
        <li>build</li>
        </ol>
        <ul>
        <li><em>build:oldest</em> library compilation with the minimum supported <code>Rust</code> version, both in debug and release mode</li>
        <li><em>build:latest</em> library compilation with the latest <code>Rust</code> version, both in debug and in release mode, it is executed also in merge request events</li>
        </ul>
        <ol start="2" type="1">
        <li>test</li>
        </ol>
        <ul>
        <li><em>test:run</em> execution of all tests present in the library with the latest <code>Rust</code> version, both in debug and in release mode, it is executed also in merge request events</li>
        <li><em>cover</em> execution of test coverage program <code>tarpaulin</code></li>
        </ul>
        <ol start="3" type="1">
        <li>examples</li>
        </ol>
        <ul>
        <li><em>examples:run</em> compilation and execution of the examples of the library using the latest <code>Rust</code> version</li>
        </ul>
        <ol start="4" type="1">
        <li>package</li>
        </ol>
        <ul>
        <li><em>package:build</em> verification of the creation of the library package</li>
        </ul>
        <ol start="5" type="1">
        <li>doc</li>
        </ol>
        <ul>
        <li><em>pages</em> creation of documentation pages in HTML format and publishing through the GitLab pages service, this job is executed only on master branch and its activation is manual</li>
        </ul>
        <ol start="6" type="1">
        <li>publish</li>
        </ol>
        <ul>
        <li><em>publish:send</em> creation of publishing on the public registry of the library package, this job is executed only on master branch and its activation is manual</li>
        </ul>
        <h1 id="references">References</h1>
        <p>[1] O. Aberth, Iteration Methods for Finding all Zeros of a Polynomial Simultaneously, Math. Comput. 27, 122 (1973) 339–344.</p>
        <p>[2] D. A. Bini, Numerical computation of polynomial zeros by means of Aberth’s method, Baltzer Journals, June 5, 1996</p>
        <p>[3] D. A. Bini, L. Robol, Solving secular and polynomial equations: A multiprecision algorithm, Journal of Computational and Applied Mathematics (2013)</p>
        <p>[4] W. S. Luk, Finding roots of real polynomial simultaneously by means of Bairstow’s method, BIT 35 (1995), 001-003</p>
        <p>[5] Donald Ervin Knuth, The Art of Computer Programming: Seminumerical algorithms, Volume 2, third edition, section 4.6.1, Algorithm D: division of polynomials over a field</p>
        <p>[6] T. H. Cormen, C. E. Leiserson, R. L. Rivest, C. Stein, Introduction to Algorithms, 3rd edition, McGraw-Hill Education, 2009</p>
        <p>[7] A. M. Andrew, Another Efficient Algorithm for Convex Hulls in Two Dimensions, Info. Proc. Letters 9, 216-219 (1979)</p>
        <p>[8] <a href="https://www.rust-lang.org/" class="uri">https://www.rust-lang.org/</a></p>
        <p>[9] <a href="https://git-scm.com/" class="uri">https://git-scm.com/</a></p>
        <p>[10] <a href="https://crates.io/" class="uri">https://crates.io/</a></p>
        <p>[11] <a href="https://forge.rust-lang.org/release/platform-support.html" class="uri">https://forge.rust-lang.org/release/platform-support.html</a></p>
        <p>[12] <a href="https://crates.io/crates/cargo-tarpaulin" class="uri">https://crates.io/crates/cargo-tarpaulin</a></p>
        <p>[13] Jenkins, M. &amp; Traub, Joseph. (1975). Principles for Testing Polynomial Zerofinding Programs. ACM Transactions on Mathematical Software (TOMS). 1. 26-34. 10.1145/355626.355632.</p>
      </div>
    </div>
  </body>
</html>
