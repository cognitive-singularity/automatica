#!/usr/bin/env sh

pandoc \
    --standalone \
    --to=html5 \
    --mathml \
    --table-of-contents --toc-depth=2 \
    --template=template.html5 \
    --output=public/index.html \
    index.md
