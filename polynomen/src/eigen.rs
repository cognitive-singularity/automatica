//! Eigenvalues and eigenvectors of a real matrix.
//!
//! The source code is a translation to Rust of Jama (Java Matrix Package).
//! Jama is released to the public domain.
//! The version used is 1.0.3 (November 9, 2012)
//! <https://math.nist.gov/javanumerics/jama/>
//!
//! If A is symmetric, then A = V*D*V' where the eigenvalue matrix D is
//! diagonal and the eigenvector matrix V is orthogonal.
//! I.e. A = V.times(D.times(V.transpose())) and
//! V.times(V.transpose()) equals the identity matrix.
//!
//! If A is not symmetric, then the eigenvalue matrix D is block diagonal
//! with the real eigenvalues in 1-by-1 blocks and any complex eigenvalues,
//! lambda + i*mu, in 2-by-2 blocks, [lambda, mu; -mu, lambda].  The
//! columns of V represent the eigenvectors in the sense that A*V = V*D,
//! i.e. A.times(V) equals V.times(D).  The matrix V may be badly
//! conditioned, or even singular, so the validity of the equation
//! A = V*D*inverse(V) depends upon V.cond().

#![allow(non_snake_case, clippy::many_single_char_names, clippy::similar_names)]

use std::ops::{Add, Div, Mul, Neg, Sub};

use crate::{Abs, Epsilon, Max, One, Sqrt, Zero};

use ndarray::{Array1, Array2};

/// Eigenvalue decomposition structure.
#[derive(Debug, Clone)]
pub(crate) struct EigenvalueDecomposition<T> {
    /// Row and column dimension (square matrix).
    n: usize,
    /// Array for internal storage of eigenvalues real part.
    d: Array1<T>,
    /// Array for internal storage of eigenvalues immaginary part.
    e: Array1<T>,
    /// Array for internal storage of nonsymmetric Hessenberg form.
    H: Array2<T>,
}

impl<T> EigenvalueDecomposition<T>
where
    T: Abs
        + Add<Output = T>
        + Clone
        + Copy
        + Div<Output = T>
        + EigenConst
        + Epsilon
        + Max
        + Mul<Output = T>
        + Neg<Output = T>
        + One
        + PartialOrd
        + Sub<Output = T>
        + Sqrt
        + Zero,
{
    /// Balance the elements of the matrix
    ///
    /// This is derived from the Algol procedures balanc,
    /// Fortran subroutines in EISPACK.
    /// Radix is chosen as in <https://github.com/scilab/scilab/blob/master/scilab/modules/elementary_functions/src/fortran/slatec/balanc.f>
    #[allow(clippy::assign_op_pattern)]
    fn balanc(&mut self) {
        let radix = T::one() + T::one();
        let b2 = radix * radix;
        let mut k: usize = 0;
        let mut l: usize = self.n;

        let swap_vector = |j_int, m_int, l_int, k_int, H: &mut Array2<T>| {
            if j_int == m_int {
                return;
            }
            // Swap column j with column m, elements with row index >= l are zero.
            for i in 0..l_int {
                H.swap((i, j_int), (i, m_int));
            }
            // Swap row j with row m, elements with column index < k are zero.
            for i in k_int..self.n {
                H.swap((j_int, i), (m_int, i));
            }
        };

        let mut j = l;
        'outer100: while j >= 1 {
            for i in 0..l {
                if i == j - 1 {
                    continue;
                };
                if !self.H[(j - 1, i)].is_zero() {
                    j = j - 1;
                    continue 'outer100;
                }
            }
            let m = l - 1; // Column/row index to swap.
            swap_vector(j - 1, m, l, k, &mut self.H);

            if l == 1 {
                return;
            }

            l = l - 1;
            j = l;
        }

        let mut j = k;
        'outer140: while j < l {
            for i in k..l {
                if i == j {
                    continue;
                };
                if !self.H[(i, j)].is_zero() {
                    j = j + 1;
                    continue 'outer140;
                }
            }
            let m = k; // Column/row index to swap.
            swap_vector(j, m, l, k, &mut self.H);

            k = k + 1;
            j = k;
        }

        let mut noconv = true;
        while noconv {
            noconv = false;
            for i in k..l {
                let mut c = T::zero();
                let mut r = T::zero();

                for j2 in k..l {
                    if j2 == i {
                        continue;
                    }
                    c = c + self.H[(j2, i)].abs();
                    r = r + self.H[(i, j2)].abs();
                }

                if c.is_zero() || r.is_zero() {
                    continue;
                }

                let mut g = r / radix;
                let mut f = T::one();
                let s = c + r;
                while c < g {
                    f = f * radix;
                    c = c * b2;
                }
                g = r * radix;
                while c >= g {
                    f = f / radix;
                    c = c / b2;
                }

                if (c + r) / f >= T::_095() * s {
                    continue;
                }

                g = T::one() / f;
                noconv = true;

                for j in k..self.n {
                    self.H[(i, j)] = self.H[(i, j)] * g;
                }
                for j in 0..l {
                    self.H[(j, i)] = self.H[(j, i)] * f;
                }
            }
        }
    }

    /// Complex scalar division.
    fn cdiv(xr: T, xi: T, yr: T, yi: T) -> (T, T) {
        let (cdivr, cdivi) = if yr.abs() > yi.abs() {
            let r = yi / yr;
            let d = yr + r * yi;
            ((xr + r * xi) / d, (xi - r * xr) / d)
        } else {
            let r = yr / yi;
            let d = yi + r * yr;
            ((r * xr + xi) / d, (r * xi - xr) / d)
        };
        (cdivr, cdivi)
    }

    /// Nonsymmetric reduction from Hessenberg to real Schur form.
    ///
    /// This is derived from the Algol procedure hqr2,
    /// by Martin and Wilkinson, Handbook for Auto. Comp.,
    /// Vol.ii-Linear Algebra, and the corresponding
    /// Fortran subroutine in EISPACK.
    #[allow(clippy::assign_op_pattern, clippy::too_many_lines)]
    fn hqr2(&mut self) {
        let two = T::one() + T::one();

        // Initialize
        let nn = self.n;
        let mut n = nn - 1;
        let eps = T::epsilon();
        let mut exshift = T::zero();
        let mut p = T::zero();
        let mut q = T::zero();
        let mut r = T::zero();
        let mut s = T::zero();
        let mut z = T::zero();
        let mut t: T;
        let mut w: T;
        let mut x: T;
        let mut y: T;

        // Store roots isolated by balanc and compute matrix norm
        let mut norm = T::zero();
        for i in 0..nn {
            for j in i.saturating_sub(1)..nn {
                norm = norm + self.H[(i, j)].abs();
            }
        }

        // Outer loop over eigenvalue index
        let mut iter = 0;
        loop {
            // Look for single small sub-diagonal element
            let mut l = n;
            while l > 0 {
                s = self.H[(l - 1, l - 1)].abs() + self.H[(l, l)].abs();
                if s.is_zero() {
                    s = norm;
                }
                if self.H[(l, l - 1)].abs() < eps * s {
                    break;
                }
                l = l - 1;
            }

            // Check for convergence
            // One root found
            if l == n {
                self.H[(n, n)] = self.H[(n, n)] + exshift;
                self.d[n] = self.H[(n, n)];
                self.e[n] = T::zero();
                if let Some(sub) = n.checked_sub(1) {
                    n = sub;
                } else {
                    break;
                }
                iter = 0;

            // Two roots found
            } else if l == n - 1 {
                w = self.H[(n, n - 1)] * self.H[(n - 1, n)];
                p = (self.H[(n - 1, n - 1)] - self.H[(n, n)]) / two;
                q = p * p + w;
                z = q.abs().sqrt();
                self.H[(n, n)] = self.H[(n, n)] + exshift;
                self.H[(n - 1, n - 1)] = self.H[(n - 1, n - 1)] + exshift;
                x = self.H[(n, n)];

                // Real pair
                if q >= T::zero() {
                    if p >= T::zero() {
                        z = p + z;
                    } else {
                        z = p - z;
                    }
                    self.d[n - 1] = x + z;
                    self.d[n] = self.d[n - 1];
                    if z != T::zero() {
                        self.d[n] = x - w / z;
                    }
                    self.e[n - 1] = T::zero();
                    self.e[n] = T::zero();
                    x = self.H[(n, n - 1)];
                    s = x.abs() + z.abs();
                    p = x / s;
                    q = z / s;
                    r = (p * p + q * q).sqrt();
                    p = p / r;
                    q = q / r;

                    // Row modification
                    for j in (n - 1)..nn {
                        z = self.H[(n - 1, j)];
                        self.H[(n - 1, j)] = q * z + p * self.H[(n, j)];
                        self.H[(n, j)] = q * self.H[(n, j)] - p * z;
                    }

                    // Column modification
                    for i in 0..=n {
                        z = self.H[(i, n - 1)];
                        self.H[(i, n - 1)] = q * z + p * self.H[(i, n)];
                        self.H[(i, n)] = q * self.H[(i, n)] - p * z;
                    }

                // Complex pair
                } else {
                    self.d[n - 1] = x + p;
                    self.d[n] = x + p;
                    self.e[n - 1] = z;
                    self.e[n] = -z;
                }
                if let Some(sub) = n.checked_sub(2) {
                    n = sub;
                } else {
                    break;
                }
                iter = 0;

            // No convergence yet
            } else {
                // Form shift
                x = self.H[(n, n)];
                y = T::zero();
                w = T::zero();
                if l < n {
                    y = self.H[(n - 1, n - 1)];
                    w = self.H[(n, n - 1)] * self.H[(n - 1, n)];
                }

                // Wilkinson's original ad hoc shift
                if iter == 10 {
                    exshift = exshift + x;
                    for i in 0..=n {
                        self.H[(i, i)] = self.H[(i, i)] - x;
                    }
                    s = self.H[(n, n - 1)].abs() + self.H[(n - 1, n - 2)].abs();
                    x = T::_075() * s;
                    y = T::_075() * s;
                    w = T::_m04375() * s * s;
                }

                // MATLAB's new ad hoc shift
                if iter == 30 {
                    s = (y - x) / two;
                    s = s * s + w;
                    if s > T::zero() {
                        s = s.sqrt();
                        if y < x {
                            s = -s;
                        }
                        s = x - w / ((y - x) / two + s);
                        for i in 0..=n {
                            self.H[(i, i)] = self.H[(i, i)] - s;
                        }
                        exshift = exshift + s;
                        x = T::_0964();
                        y = T::_0964();
                        w = T::_0964();
                    }
                }

                iter = iter + 1; // (Could check iteration count here.)
                let MAX_ITERATIONS = 100;
                if iter > MAX_ITERATIONS {
                    eprintln!(
                        "The algorithm hqr2 has not converged after {} iterations",
                        MAX_ITERATIONS
                    );
                    break;
                }

                // Look for two consecutive small sub-diagonal elements
                let mut m = n - 2;
                while m >= l {
                    z = self.H[(m, m)];
                    r = x - z;
                    s = y - z;
                    p = (r * s - w) / self.H[(m + 1, m)] + self.H[(m, m + 1)];
                    q = self.H[(m + 1, m + 1)] - z - r - s;
                    r = self.H[(m + 2, m + 1)];
                    s = p.abs() + q.abs() + r.abs();
                    p = p / s;
                    q = q / s;
                    r = r / s;
                    if m == l {
                        break;
                    }
                    if self.H[(m, m - 1)].abs() * (q.abs() + r.abs())
                        < eps
                            * (p.abs()
                                * (self.H[(m - 1, m - 1)].abs()
                                    + z.abs()
                                    + self.H[(m + 1, m + 1)].abs()))
                    {
                        break;
                    }
                    m = m - 1;
                }

                for i in (m + 2)..=n {
                    self.H[(i, i - 2)] = T::zero();
                    if i > m + 2 {
                        self.H[(i, i - 3)] = T::zero();
                    }
                }

                // Double QR step involving rows l:n and columns m:n
                for k in m..n {
                    let notlast: bool = k != n - 1;
                    if k != m {
                        p = self.H[(k, k - 1)];
                        q = self.H[(k + 1, k - 1)];
                        r = if notlast {
                            self.H[(k + 2, k - 1)]
                        } else {
                            T::zero()
                        };
                        x = p.abs() + q.abs() + r.abs();
                        if x.is_zero() {
                            continue;
                        }
                        p = p / x;
                        q = q / x;
                        r = r / x;
                    }

                    s = (p * p + q * q + r * r).sqrt();
                    if p < T::zero() {
                        s = -s;
                    }
                    if s != T::zero() {
                        if k != m {
                            self.H[(k, k - 1)] = -s * x;
                        } else if l != m {
                            self.H[(k, k - 1)] = -self.H[(k, k - 1)];
                        }
                        p = p + s;
                        x = p / s;
                        y = q / s;
                        z = r / s;
                        q = q / p;
                        r = r / p;

                        // Row modification
                        for j in k..nn {
                            p = self.H[(k, j)] + q * self.H[(k + 1, j)];
                            if notlast {
                                p = p + r * self.H[(k + 2, j)];
                                self.H[(k + 2, j)] = self.H[(k + 2, j)] - p * z;
                            }
                            self.H[(k, j)] = self.H[(k, j)] - p * x;
                            self.H[(k + 1, j)] = self.H[(k + 1, j)] - p * y;
                        }

                        // Column modification
                        for i in 0..=n.min(k + 3) {
                            p = x * self.H[(i, k)] + y * self.H[(i, k + 1)];
                            if notlast {
                                p = p + z * self.H[(i, k + 2)];
                                self.H[(i, k + 2)] = self.H[(i, k + 2)] - p * r;
                            }
                            self.H[(i, k)] = self.H[(i, k)] - p;
                            self.H[(i, k + 1)] = self.H[(i, k + 1)] - p * q;
                        }
                    } // (s != 0)
                } // k loop
            } // check convergence
        } // while (n >= 0)

        // Backsubstitute to find vectors of upper triangular form
        if norm.is_zero() {
            return;
        }

        for n in (0..nn).rev() {
            p = self.d[n];
            q = self.e[n];

            // Real vector
            if q.is_zero() {
                let mut l = n;
                self.H[(n, n)] = T::one();
                for i in (0..n).rev() {
                    w = self.H[(i, i)] - p;
                    r = T::zero();
                    for j in l..=n {
                        r = r + self.H[(i, j)] * self.H[(j, n)];
                    }
                    if self.e[i] < T::zero() {
                        z = w;
                        s = r;
                    } else {
                        l = i;
                        if self.e[i].is_zero() {
                            if w.is_zero() {
                                self.H[(i, n)] = -r / (eps * norm);
                            } else {
                                self.H[(i, n)] = -r / w;
                            }

                        // Solve real equations
                        } else {
                            x = self.H[(i, i + 1)];
                            y = self.H[(i + 1, i)];
                            q = (self.d[i] - p) * (self.d[i] - p) + self.e[i] * self.e[i];
                            t = (x * s - z * r) / q;
                            self.H[(i, n)] = t;
                            if x.abs() > z.abs() {
                                self.H[(i + 1, n)] = (-r - w * t) / x;
                            } else {
                                self.H[(i + 1, n)] = (-s - y * t) / z;
                            }
                        }

                        // Overflow control
                        t = self.H[(i, n)].abs();
                        if (eps * t) * t > T::one() {
                            for j in i..=n {
                                self.H[(j, n)] = self.H[(j, n)] / t;
                            }
                        }
                    }
                }

            // Complex vector
            } else if q < T::zero() {
                let mut l = n - 1;

                // Last vector component imaginary so matrix is triangular
                if self.H[(n, n - 1)].abs() > self.H[(n - 1, n)].abs() {
                    self.H[(n - 1, n - 1)] = q / self.H[(n, n - 1)];
                    self.H[(n - 1, n)] = -(self.H[(n, n)] - p) / self.H[(n, n - 1)];
                } else {
                    let (cdivr, cdivi) = Self::cdiv(
                        T::zero(),
                        -self.H[(n - 1, n)],
                        self.H[(n - 1, n - 1)] - p,
                        q,
                    );
                    self.H[(n - 1, n - 1)] = cdivr;
                    self.H[(n - 1, n)] = cdivi;
                }
                self.H[(n, n - 1)] = T::zero();
                self.H[(n, n)] = T::one();
                for i in (1..n).rev() {
                    let i = i - 1; // Avoid usize underflow
                    let mut ra: T;
                    let mut sa: T;
                    let mut vr: T;
                    let vi: T;
                    ra = T::zero();
                    sa = T::zero();
                    for j in l..=n {
                        ra = ra + self.H[(i, j)] * self.H[(j, n - 1)];
                        sa = sa + self.H[(i, j)] * self.H[(j, n)];
                    }
                    w = self.H[(i, i)] - p;

                    if self.e[i] < T::zero() {
                        z = w;
                        r = ra;
                        s = sa;
                    } else {
                        l = i;
                        if self.e[i].is_zero() {
                            let (cdivr, cdivi) = Self::cdiv(-ra, -sa, w, q);
                            self.H[(i, n - 1)] = cdivr;
                            self.H[(i, n)] = cdivi;
                        } else {
                            // Solve complex equations
                            x = self.H[(i, i + 1)];
                            y = self.H[(i + 1, i)];
                            vr = (self.d[i] - p) * (self.d[i] - p) + self.e[i] * self.e[i] - q * q;
                            vi = (self.d[i] - p) * two * q;
                            if vr.is_zero() && vi.is_zero() {
                                vr = eps * norm * (w.abs() + q.abs() + x.abs() + y.abs() + z.abs());
                            }
                            let (cdivr, cdivi) = Self::cdiv(
                                x * r - z * ra + q * sa,
                                x * s - z * sa - q * ra,
                                vr,
                                vi,
                            );
                            self.H[(i, n - 1)] = cdivr;
                            self.H[(i, n)] = cdivi;
                            if x.abs() > (z.abs() + q.abs()) {
                                self.H[(i + 1, n - 1)] =
                                    (-ra - w * self.H[(i, n - 1)] + q * self.H[(i, n)]) / x;
                                self.H[(i + 1, n)] =
                                    (-sa - w * self.H[(i, n)] - q * self.H[(i, n - 1)]) / x;
                            } else {
                                let (cdivr, cdivi) = Self::cdiv(
                                    -r - y * self.H[(i, n - 1)],
                                    -s - y * self.H[(i, n)],
                                    z,
                                    q,
                                );
                                self.H[(i + 1, n - 1)] = cdivr;
                                self.H[(i + 1, n)] = cdivi;
                            }
                        }

                        // Overflow control
                        t = self.H[(i, n - 1)].abs().max(self.H[(i, n)].abs());
                        if (eps * t) * t > T::one() {
                            for j in i..=n {
                                self.H[(j, n - 1)] = self.H[(j, n - 1)] / t;
                                self.H[(j, n)] = self.H[(j, n)] / t;
                            }
                        }
                    }
                }
            }
        }
    }

    /// Check for symmetry, then construct the eigenvalue decomposition
    /// structure to access D and V.
    ///
    /// # Arguments
    ///
    /// * `Arg` - Square matrix
    ///
    /// # Example
    /// ```text
    /// use ndarray::arr2;
    /// let A = arr2(&[[4., 1., 1.], [1., 2., 3.], [1., 3., 6.]]);
    /// let eig = EigenvalueDecomposition::new(&A);
    /// ```
    pub(crate) fn new(Arg: &Array2<T>) -> Self {
        let A: Array2<T> = Arg.clone();
        let n = Arg.ncols();

        let mut evd = EigenvalueDecomposition {
            n,
            d: Array1::from_elem(n, T::zero()),
            e: Array1::from_elem(n, T::zero()),
            H: A,
        };

        // Balance the elements of the matrix.
        evd.balanc();

        // Reduce Hessenberg to real Schur form.
        evd.hqr2();
        evd
    }

    /// Return the real parts of the eigenvalues: real(diag(D))
    pub(crate) fn get_real_eigenvalues(&self) -> Vec<T> {
        self.d.to_vec()
    }

    /// Return the imaginary parts of the eigenvalues: imag(diag(D))
    pub(crate) fn get_imag_eigenvalues(&self) -> Vec<T> {
        self.e.to_vec()
    }

    /// Return the complex eigenvalues: diag(D)
    pub(crate) fn get_eigenvalues(&self) -> Vec<(T, T)> {
        ndarray::Zip::from(&self.d)
            .and(&self.e)
            .map_collect(|&x, &y| (x, y))
            .to_vec()
    }
}

/// Trait that defines the constants used in the Eigen solver.
#[allow(clippy::module_name_repetitions)]
pub trait EigenConst {
    /// 0.75 constant
    fn _075() -> Self;
    /// -0.4375 constant
    fn _m04375() -> Self;
    /// 0.964 constant
    fn _0964() -> Self;
    /// 0.95 constant
    fn _095() -> Self;
}

macro_rules! impl_eigen_const {
    ($t:ty) => {
        impl EigenConst for $t {
            fn _075() -> Self {
                0.75
            }
            fn _m04375() -> Self {
                -0.4375
            }
            fn _0964() -> Self {
                0.964
            }
            fn _095() -> Self {
                0.95
            }
        }
    };
}

impl_eigen_const!(f32);
impl_eigen_const!(f64);

#[cfg(test)]
mod tests {
    use super::*;
    use ndarray::arr2;

    #[test]
    fn single_value_matrix() {
        let A = arr2(&[[17.]]);
        let eig = EigenvalueDecomposition::new(&A);
        let d = eig.get_eigenvalues();

        assert_relative_eq!(17., d[0].0);
        assert_relative_eq!(0., d[0].1);
    }

    #[test]
    fn non_symmetric_matrix() {
        // p(x) = 20 - 41x + 29x^2 - 9x^3 + x^4
        let A = arr2(&[
            [0., 0., 0., -20.],
            [1., 0., 0., 41.],
            [0., 1., 0., -29.],
            [0., 0., 1., 9.],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let mut d = eig.get_eigenvalues();

        let roots = [(1., 0.), (2., -1.), (2., 1.), (4., 0.)];
        d.sort_by(|&x, &y| {
            x.0.partial_cmp(&y.0)
                .unwrap()
                .then(x.1.partial_cmp(&y.1).unwrap())
        });
        for (i, r) in d.iter().zip(&roots) {
            assert_relative_eq!(i.0, r.0, max_relative = 1e-10);
            assert_relative_eq!(i.1, r.1, max_relative = 1e-10);
        }
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn multiple_zeros() {
        // p(x) = -x^4 + x^5 = x^4(x - 1)
        let bA = arr2(&[
            [0., 0., 0., 0., 0.],
            [1., 0., 0., 0., 0.],
            [0., 1., 0., 0., 0.],
            [0., 0., 1., 0., 0.],
            [0., 0., 0., 1., 1.],
        ]);
        let eig = EigenvalueDecomposition::new(&bA);
        let d = eig.get_real_eigenvalues();
        let e = eig.get_imag_eigenvalues();
        assert_eq!(1., d.iter().sum::<f64>());
        assert!(e.iter().all(|x| *x == 0.));
    }

    #[test]
    fn multiple_real_roots() {
        // p(x) = -72 +60x +10x^2 -15x^3 +x^5
        let bA = arr2(&[
            [0., 0., 0., 0., 72.],
            [1., 0., 0., 0., -60.],
            [0., 1., 0., 0., -10.],
            [0., 0., 1., 0., 15.],
            [0., 0., 0., 1., 0.],
        ]);
        let eig = EigenvalueDecomposition::new(&bA);
        let mut d = eig.get_eigenvalues();
        let roots = [(-3., 0.), (-3., 0.), (2., 0.), (2., 0.), (2., 0.)];
        d.sort_by(|&x, &y| {
            x.0.partial_cmp(&y.0)
                .unwrap()
                .then(x.1.partial_cmp(&y.1).unwrap())
        });
        for (i, r) in d.iter().zip(&roots) {
            assert_relative_eq!(i.0, r.0, max_relative = 1e-5);
            assert_relative_eq!(i.1, r.1, epsilon = 1e-4);
        }
    }

    #[test]
    fn multiple_complex_roots() {
        // p(x) = -72 +60x +10x^2 -15x^3 +x^5
        let bA = arr2(&[
            [0., 0., 0., -841.],
            [1., 0., 0., -580.],
            [0., 1., 0., -158.],
            [0., 0., 1., -20.],
        ]);
        let eig = EigenvalueDecomposition::new(&bA);
        let mut d = eig.get_eigenvalues();
        let roots = [(-5., -2.), (-5., -2.), (-5., 2.), (-5., 2.)];
        d.sort_by(|&x, &y| x.1.partial_cmp(&y.1).unwrap());
        dbg!(&d);
        for (i, r) in d.iter().zip(&roots) {
            assert_relative_eq!(i.0, r.0, max_relative = 1e-7);
            assert_relative_eq!(i.1, r.1, epsilon = 1e-6);
        }
    }

    #[test]
    #[allow(clippy::excessive_precision)]
    fn no_endless_loop() {
        // The following matrix eigenvalue decomposition is unstable for some
        // libraries if the numeric type is f32, in case of f64 the decomposition
        // it returns the correct values.
        // This test is to check that the algorithm return (even if the result
        // is wrong) and it is not stuck inside an endless loop.
        let A = arr2(&[
            [
                0.0_f32,
                0.,
                0.,
                0.,
                0.,
                0.,
                0.,
                0.,
                0.,
                0.,
                -6.249_999_97e+10,
            ],
            [1., 0., 0., 0., 0., 0., 0., 0., 0., 0., -1.263_125e+12],
            [0., 1., 0., 0., 0., 0., 0., 0., 0., 0., -6.828_475_29e+12],
            [0., 0., 1., 0., 0., 0., 0., 0., 0., 0., -2.038_578_54e+12],
            [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., -7.359_804_21e+11],
            [0., 0., 0., 0., 1., 0., 0., 0., 0., 0., -7.862_141_75e+10],
            [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., -1.056_018_12e+10],
            [0., 0., 0., 0., 0., 0., 1., 0., 0., 0., -154_605_568.],
            [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., -4_472_637.],
            [0., 0., 0., 0., 0., 0., 0., 0., 1., 0., -33_713.007_8],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., -440.200_012],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let mut lambda = eig.get_eigenvalues();
        lambda.sort_by(|&x, &y| {
            x.0.partial_cmp(&y.0)
                .unwrap()
                .then(x.1.partial_cmp(&y.1).unwrap())
        });
        assert_relative_eq!(-380.15, lambda[0].0, max_relative = 10e-6);
        assert_relative_eq!(0., lambda[0].1);
    }
}
