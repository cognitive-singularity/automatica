//! Polynomial evaluation methods

use std::ops::{Add, Div, Mul, Neg};

use crate::{One, Poly, Zero};

// Evaluate the polynomial at the given real or complex number
// impl<N, T> Eval<N> for Poly<T>
// where
//     N: Copy + MulAdd<Output = N> + NumCast + Zero,
//     T: Copy + NumCast,
// {
//     /// Evaluate the polynomial using Horner's method. The evaluation is safe
//     /// if the polynomial coefficient can be casted the type `N`.
//     ///
//     /// # Arguments
//     ///
//     /// * `x` - Value at which the polynomial is evaluated.
//     ///
//     /// # Panics
//     ///
//     /// The method panics if the conversion from `T` to type `N` fails.
//     ///
//     /// # Example
//     /// ```
//     /// use automatica::{Eval, num_complex::Complex, polynomial::Poly};
//     /// let p = Poly::new_from_coeffs(&[0., 0., 2.]);
//     /// assert_eq!(18., p.eval(3.));
//     /// assert_eq!(Complex::new(-18., 0.), p.eval(Complex::new(0., 3.)));
//     /// ```
//     fn eval_ref(&self, x: &N) -> N {
//         self.coeffs
//             .iter()
//             .rev()
//             .fold(N::zero(), |acc, &c| acc.mul_add(*x, N::from(c).unwrap()))
//     }
// }

impl<T: Clone> Poly<T> {
    // The current implementation relies on the ability to add type N and T.
    // When the trait MulAdd<N,T> for N=Complex<T>, mul_add may be used.
    /// Evaluate the polynomial using Horner's method.
    ///
    /// # Arguments
    ///
    /// * `x` - Value at which the polynomial is evaluated.
    ///
    /// # Example
    /// ```
    /// use polynomen::Poly;
    /// let p = Poly::new_from_coeffs(&[0., 0., 2.]);
    /// assert_eq!(18., p.eval_by_val(3.));
    /// ```
    pub fn eval_by_val<U>(&self, x: U) -> U
    where
        U: Add<T, Output = U> + Clone + Mul<U, Output = U> + Zero,
    {
        self.coeffs
            .iter()
            .rev()
            .fold(U::zero(), |acc, c| acc * x.clone() + c.clone())
    }
}

impl<T> Poly<T> {
    /// Evaluate the polynomial using Horner's method.
    ///
    /// # Arguments
    ///
    /// * `x` - Value at which the polynomial is evaluated.
    ///
    /// # Example
    /// ```
    /// use polynomen::Poly;
    /// let p = Poly::new_from_coeffs(&[0., 0., 2.]);
    /// assert_eq!(18., p.eval(&3.));
    /// ```
    pub fn eval<'a, 'b, U>(&'a self, x: &'b U) -> U
    where
        U: Add<&'a T, Output = U> + Mul<&'b U, Output = U> + Zero,
    {
        // Both the polynomial and the input value must have the same lifetime.
        self.coeffs
            .iter()
            .rev()
            .fold(U::zero(), |acc, c| acc * x + c)
    }
}

/// Evaluate the ratio between to polynomials at the given value.
/// This implementation avoids overflow issues when evaluating the
/// numerator and the denominator separately.
///
/// # Arguments
///
/// * `numerator` - numerator of the polynomial ratio.
/// * `denominator` - denominator of the polynomial ratio.
/// * `x` - Value at which the polynomial ratio is evaluated.
///
/// # Example
/// ```
/// use polynomen::Poly;
/// let p1 = Poly::new_from_coeffs(&[4., 5., 1.]);
/// let p2 = Poly::new_from_coeffs(&[1., 2., 3., 1.]);
/// let x = -1e30_f32;
/// let r = polynomen::eval_poly_ratio(&p1, &p2, x);
/// let naive = p1.eval(&x) / p2.eval(&x);
/// assert!(naive.is_nan());
/// assert!((0.- r).abs() < 1e-16);
/// ```
pub fn eval_poly_ratio<T, N>(numerator: &Poly<T>, denominator: &Poly<T>, x: N) -> N
where
    N: Add<T, Output = N>
        + Clone
        + Div<Output = N>
        + Mul<Output = N>
        + Neg<Output = N>
        + One
        + PartialOrd
        + Zero,
    T: Add<Output = T> + Clone + Zero,
{
    // When the `x` value is greater than one evaluate the polynomial ratio
    // at `1/x` reversing the coefficients.
    if -N::one() <= x && x <= N::one() {
        let n = numerator.eval_by_val(x.clone());
        let d = denominator.eval_by_val(x);
        n / d
    } else {
        let x = N::one() / x;
        // Zip and extend the smaller polynomial with zeros.
        // Evaluate the reversed polynomial.
        let (n, d) = zip_fill::zip_longest(&numerator.coeffs, &denominator.coeffs, &T::zero())
            .fold((N::zero(), N::zero()), |acc, c| {
                (
                    acc.0 * x.clone() + c.0.clone(),
                    acc.1 * x.clone() + c.1.clone(),
                )
            });
        n / d
    }
}

#[cfg(test)]
mod tests {
    use crate::{complex::Complex, poly};

    use super::*;

    #[test]
    fn poly_eval() {
        let p = poly!(1., 2., 3.);
        assert_abs_diff_eq!(86., p.eval(&5.), epsilon = 0.);

        assert_abs_diff_eq!(0., Poly::<f64>::zero().eval(&6.4), epsilon = 0.);

        let p2 = poly!(3, 4, 1);
        assert_eq!(143, p2.eval(&10));
    }

    #[allow(clippy::similar_names)]
    #[test]
    fn poly_eval_loss_precision() {
        // Using f32 instead of f64 the polynomial evaluation looses precision.
        let p1f32 = Poly::new_from_coeffs(&[-1e-20_f32, 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]);
        let p2f32 = Poly::new_from_coeffs(&[1e20_f32, 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]);
        let pf32 = p1f32 * p2f32;
        let derf32 = pf32.derive();

        let a = derf32.eval(&Complex::new(80.9017, 58.77853));
        assert!(a.re.x.is_infinite());

        let p1f64 = Poly::new_from_coeffs(&[-1e-20_f64, 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]);
        let p2f64 = Poly::new_from_coeffs(&[1e20_f64, 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]);
        let pf64 = p1f64 * p2f64;
        let derf64 = pf64.derive();

        let b = derf64.eval(&Complex::new(80.9017, 58.77853));
        assert!(b.re.x.is_finite());
    }

    #[test]
    fn poly_cmplx_eval() {
        let p = poly!(1., 1., 1.);
        let c = Complex::new(1.0, 1.0);
        let res = Complex::new(2.0, 3.0);
        assert_eq!(res, p.eval(&c));

        assert_eq!(
            Complex::zero(),
            Poly::<f64>::new_from_coeffs(&[]).eval(&Complex::new(2., 3.))
        );
    }

    #[test]
    fn poly_eval_by_value() {
        let p = poly!(1., 2., 3.);
        let r1 = p.eval_by_val(0.);
        let r2 = p.eval(&0.);
        assert_relative_eq!(r1, r2);
    }

    #[test]
    fn eval_poly_of_poly() {
        let s = poly!(-1, 1);
        let p = poly!(1, 2, 3);
        let r = p.eval(&s);
        assert_eq!(poly!(2, -4, 3), r);
    }

    #[test]
    fn poly_ratio_evaluation() {
        let p1 = poly!(1., 2., 3.);
        let p2 = poly!(4., 5.);
        let x = 2.;
        let r1 = eval_poly_ratio(&p1, &p2, x);
        assert_relative_eq!(p1.eval(&x) / p2.eval(&x), r1);

        let y = 0.5;
        let r2 = eval_poly_ratio(&p1, &p2, y);
        assert_relative_eq!(p1.eval(&y) / p2.eval(&y), r2);
    }

    #[test]
    fn poly_ratio_overflow() {
        let p1 = Poly::new_from_coeffs(&[4., 5., 1.]);
        let p2 = Poly::new_from_coeffs(&[1., 2., 3., 1.]);
        let x = -1e30_f32;
        let r = eval_poly_ratio(&p1, &p2, x);
        let naive = p1.eval(&x) / p2.eval(&x);
        assert!(naive.is_nan());
        assert!((0. - r).abs() < 1e-16);
    }
}
