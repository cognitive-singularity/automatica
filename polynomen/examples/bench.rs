use std::time::Instant;

use polynomen::Poly;

fn main() {
    println!("Loss of precision multiplication");
    let (convolution, result, fft, _) = bench_1();
    print_result(convolution, fft);
    println!("{:.1e}", result);

    println!("24th degree multiplication");
    let (convolution2, _, fft2, _) = bench_2();
    print_result(convolution2, fft2);

    println!("100th degree multiplication");
    let (convolution3, _, fft3, _) = bench_n(100);
    print_result(convolution3, fft3);

    println!("1000th degree multiplication");
    let (convolution4, _, fft4, _) = bench_n(1_000);
    print_result(convolution4, fft4);

    println!("2000th degree multiplication");
    let (convolution5, _, fft5, _) = bench_n(2_000);
    print_result(convolution5, fft5);
}

fn print_result(convolution_time: u128, fft_time: u128) {
    println!(
        "Convolution: {} \u{b5}s, fft: {} \u{b5}s",
        convolution_time, fft_time
    );
}

/// Bench the multiplication of two Wilkinson's polynomial.
fn bench_1() -> (u128, Poly<f64>, u128, Poly<f64>) {
    let wp = Poly::new_from_roots(&[1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.]);

    let start = Instant::now();
    let result1 = &wp * &wp;
    let first_step = start.elapsed().as_micros();

    let wp2 = wp.clone();

    let later = Instant::now();
    let result2 = wp.mul_fft(wp2);
    let second_step = later.elapsed().as_micros();

    (first_step, result1, second_step, result2)
}

/// Bench the multiplication of two 24 degree polynomials.
fn bench_2() -> (u128, Poly<f64>, u128, Poly<f64>) {
    // points = [[1:24],zeros(1,40)]; res=ifft(fft(points)^2); res(1:47)
    let wp = Poly::new_from_coeffs(&[
        1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16., 17., 18., 19., 20.,
        21., 22., 23., 24.,
    ]);

    let start = Instant::now();
    let result1 = &wp * &wp;
    let first_step = start.elapsed().as_micros();

    let wp2 = wp.clone();

    let later = Instant::now();
    let result2 = wp.mul_fft(wp2);
    let second_step = later.elapsed().as_micros();

    (first_step, result1, second_step, result2)
}

/// Bench the multiplication of two polynomials of the given degree.
fn bench_n(degree: usize) -> (u128, Poly<f64>, u128, Poly<f64>) {
    let wp = Poly::new_from_coeffs_iter(std::iter::repeat(2.0).take(degree));

    let start = Instant::now();
    let result1 = &wp * &wp;
    let first_step = start.elapsed().as_micros();

    let wp2 = wp.clone();

    let later = Instant::now();
    let result2 = wp.mul_fft(wp2);
    let second_step = later.elapsed().as_micros();

    (first_step, result1, second_step, result2)
}
