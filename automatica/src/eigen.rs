//! Eigenvalues and eigenvectors of a real matrix.
//!
//! The source code is a translation to Rust of Jama (Java Matrix Package).
//! Jama is released to the public domain.
//! The version used is 1.0.3 (November 9, 2012)
//! <https://math.nist.gov/javanumerics/jama/>
//!
//! If A is symmetric, then A = V*D*V' where the eigenvalue matrix D is
//! diagonal and the eigenvector matrix V is orthogonal.
//! I.e. A = V.times(D.times(V.transpose())) and
//! V.times(V.transpose()) equals the identity matrix.
//!
//! If A is not symmetric, then the eigenvalue matrix D is block diagonal
//! with the real eigenvalues in 1-by-1 blocks and any complex eigenvalues,
//! lambda + i*mu, in 2-by-2 blocks, [lambda, mu; -mu, lambda].  The
//! columns of V represent the eigenvectors in the sense that A*V = V*D,
//! i.e. A.times(V) equals V.times(D).  The matrix V may be badly
//! conditioned, or even singular, so the validity of the equation
//! A = V*D*inverse(V) depends upon V.cond().

#![allow(
    non_snake_case,
    dead_code,
    clippy::many_single_char_names,
    clippy::similar_names
)]

use std::ops::{Add, Div, Mul, Neg, Sub};

use crate::{Abs, Epsilon, Hypot, Max, One, Pow, Sqrt, Zero};

use ndarray::{Array1, Array2};

#[derive(Debug, Clone, PartialEq)]
enum Scale<T> {
    Factor(T),
    Index(usize),
}

/// Eigenvalue decomposition structure.
#[derive(Debug, Clone)]
pub(crate) struct EigenvalueDecomposition<T> {
    /// Row and column dimension (square matrix).
    n: usize,
    /// Symmetry flag.
    issymmetric: bool,
    /// Array for internal storage of eigenvalues real part.
    d: Array1<T>,
    /// Array for internal storage of eigenvalues immaginary part.
    e: Array1<T>,
    /// Array for internal storage of eigenvectors.
    V: Array2<T>,
    /// Array for internal storage of nonsymmetric Hessenberg form.
    H: Array2<T>,
    /// Working storage for nonsymmetric algorithm.
    ort: Array1<T>,
    /// Scaling factors for balanc algorithm.
    scale: Array1<Scale<T>>,
    low: usize,
    high: usize,
}

impl<T> EigenvalueDecomposition<T>
where
    T: Abs
        + Add<Output = T>
        + Clone
        + Copy
        + Div<Output = T>
        + EigenConst
        + Epsilon
        + Hypot
        + Max
        + Mul<Output = T>
        + Neg<Output = T>
        + One
        + PartialOrd
        + Pow<T>
        + Sub<Output = T>
        + Sqrt
        + Zero,
{
    /// Balance the elements of the matrix
    ///
    /// This is derived from the Algol procedures balanc,
    /// Fortran subroutines in EISPACK.
    /// Radix is chosen as in <https://github.com/scilab/scilab/blob/master/scilab/modules/elementary_functions/src/fortran/slatec/balanc.f>
    #[allow(clippy::assign_op_pattern, clippy::too_many_lines)]
    fn balanc(&mut self) {
        let radix = T::one() + T::one();
        let b2 = radix * radix;
        let mut k: usize = 0;
        let mut l: usize = self.n;

        let mut swap_vector = |j_int, m_int, l_int, k_int, H: &mut Array2<T>| {
            self.scale[m_int] = Scale::Index(j_int);
            if j_int == m_int {
                return;
            }
            // Swap column j with column m, elements with row index >= l are zero.
            for i in 0..l_int {
                H.swap((i, j_int), (i, m_int));
            }
            // Swap row j with row m, elements with column index < k are zero.
            for i in k_int..self.n {
                H.swap((j_int, i), (m_int, i));
            }
        };

        let mut j = l;
        'outer100: while j >= 1 {
            for i in 0..l {
                if i == j - 1 {
                    continue;
                };
                if !self.H[(j - 1, i)].is_zero() {
                    j = j - 1;
                    continue 'outer100;
                }
            }
            let m = l - 1; // Column/row index to swap.
            swap_vector(j - 1, m, l, k, &mut self.H);

            if l == 1 {
                self.low = k;
                self.high = l;
                return;
            }

            // Restart the loop removing the bottom row and rightmost column.
            l = l - 1;
            j = l;
        }

        let mut j = k;
        'outer140: while j < l {
            for i in k..l {
                if i == j {
                    continue;
                };
                if !self.H[(i, j)].is_zero() {
                    j = j + 1;
                    continue 'outer140;
                }
            }
            let m = k; // Column/row index to swap.
            swap_vector(j, m, l, k, &mut self.H);

            // Restart the loop removing the top row and leftmost column.
            k = k + 1;
            j = k;
        }

        for i in k..l {
            self.scale[i] = Scale::Factor(T::one());
        }

        let mut noconv = true;
        while noconv {
            noconv = false;
            for i in k..l {
                let mut c = T::zero();
                let mut r = T::zero();

                for j2 in k..l {
                    if j2 == i {
                        continue;
                    }
                    c = c + self.H[(j2, i)].abs();
                    r = r + self.H[(i, j2)].abs();
                }

                if c.is_zero() || r.is_zero() {
                    continue;
                }

                let mut g = r / radix;
                let mut f = T::one();
                let s = c + r;
                while c < g {
                    f = f * radix;
                    c = c * b2;
                }
                g = r * radix;
                while c >= g {
                    f = f / radix;
                    c = c / b2;
                }

                if (c + r) / f >= T::_095() * s {
                    continue;
                }

                g = T::one() / f;
                if let Scale::Factor(old) = self.scale[i] {
                    self.scale[i] = Scale::Factor(old * f);
                }
                noconv = true;

                for j in k..self.n {
                    self.H[(i, j)] = self.H[(i, j)] * g;
                }
                for j in 0..l {
                    self.H[(j, i)] = self.H[(j, i)] * f;
                }
            }
        }

        self.low = k;
        self.high = l;
    }

    /// This subroutine forms the eigenvectors of a real general
    /// matrix by back transforming those of the corresponding
    /// balanced matrix determined by  balanc.
    /// Fortran subroutines in EISPACK.
    fn balbak(&mut self) {
        let low = self.low;
        let high = self.high;
        let m = self.n;
        if m == 0 {
            return;
        }

        // When low == high this loop is not run.
        for i in low..high {
            if let Scale::Factor(s) = self.scale[i] {
                for j in 0..m {
                    self.V[(i, j)] = self.V[(i, j)] * s;
                }
            }
        }

        for ii in 0..self.n {
            let mut i = ii;
            if i >= low && i < high {
                continue;
            }
            if i < low {
                i = low - ii;
            }
            if let Scale::Index(k) = self.scale[i] {
                if k == i {
                    continue;
                }
                for j in 0..m {
                    // Swap row i with row k.
                    self.V.swap((i, j), (k, j));
                }
            }
        }
    }

    /// Symmetric Householder reduction to tridiagonal form.
    ///
    /// This is derived from the Algol procedures tred2 by
    /// Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
    /// Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
    /// Fortran subroutine in EISPACK.
    fn tred2(&mut self) {
        for j in 0..self.n {
            self.d[j] = self.V[(self.n - 1, j)];
        }

        // Householder reduction to tridiagonal form.
        for i in (1..self.n).rev() {
            // Scale to avoid under/overflow.
            let mut scale = T::zero();
            let mut h = T::zero();
            for k in 0..i {
                scale = scale + self.d[k].abs();
            }
            if scale.is_zero() {
                self.e[i] = self.d[i - 1];
                for j in 0..i {
                    self.d[j] = self.V[(i - 1, j)];
                    self.V[(i, j)] = T::zero();
                    self.V[(j, i)] = T::zero();
                }
            } else {
                // Generate Householder vector.
                for k in 0..i {
                    self.d[k] = self.d[k] / scale;
                    h = h + self.d[k] * self.d[k];
                }
                let mut f = self.d[i - 1];
                let mut g = h.sqrt();
                if f > T::zero() {
                    g = -g;
                }
                self.e[i] = scale * g;
                h = h - f * g;
                self.d[i - 1] = f - g;
                for j in 0..i {
                    self.e[j] = T::zero();
                }

                // Apply similarity transformation to remaining columns.
                for j in 0..i {
                    f = self.d[j];
                    self.V[(j, i)] = f;
                    g = self.e[j] + self.V[(j, j)] * f;
                    for k in (j + 1)..i {
                        g = g + self.V[(k, j)] * self.d[k];
                        self.e[k] = self.e[k] + self.V[(k, j)] * f;
                    }
                    self.e[j] = g;
                }
                f = T::zero();
                for j in 0..i {
                    self.e[j] = self.e[j] / h;
                    f = f + self.e[j] * self.d[j];
                }
                let hh = f / (h + h);
                for j in 0..i {
                    self.e[j] = self.e[j] - hh * self.d[j];
                }
                for j in 0..i {
                    f = self.d[j];
                    g = self.e[j];
                    for k in j..=i {
                        self.V[(k, j)] = self.V[(k, j)] - (f * self.e[k] + g * self.d[k]);
                    }
                    self.d[j] = self.V[(i - 1, j)];
                    self.V[(i, j)] = T::zero();
                }
            }
            self.d[i] = h;
        }

        // Accumulate transformations.
        for i in 0..(self.n - 1) {
            self.V[(self.n - 1, i)] = self.V[(i, i)];
            self.V[(i, i)] = T::one();
            let h = self.d[i + 1];
            if h != T::zero() {
                for k in 0..=i {
                    self.d[k] = self.V[(k, i + 1)] / h;
                }
                for j in 0..=i {
                    let mut g = T::zero();
                    for k in 0..=i {
                        g = g + self.V[(k, i + 1)] * self.V[(k, j)];
                    }
                    for k in 0..=i {
                        self.V[(k, j)] = self.V[(k, j)] - g * self.d[k];
                    }
                }
            }
            for k in 0..=i {
                self.V[(k, i + 1)] = T::zero();
            }
        }
        for j in 0..self.n {
            self.d[j] = self.V[(self.n - 1, j)];
            self.V[(self.n - 1, j)] = T::zero();
        }
        self.V[(self.n - 1, self.n - 1)] = T::one();
        self.e[0] = T::zero();
    }

    /// Symmetric tridiagonal QL algorithm.
    ///
    /// This is derived from the Algol procedures tql2, by
    /// Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
    /// Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
    /// Fortran subroutine in EISPACK.
    #[allow(clippy::assign_op_pattern)]
    fn tql2(&mut self) {
        let two = T::one() + T::one();

        for i in 1..self.n {
            self.e[i - 1] = self.e[i];
        }
        self.e[self.n - 1] = T::zero();

        let mut f = T::zero();
        let mut tst1 = T::zero();
        let eps = T::epsilon();
        for l in 0..self.n {
            // Find small subdiagonal element
            tst1 = tst1.max(&(self.d[l].abs() + self.e[l].abs()));
            let mut m = l;
            while m < self.n {
                if self.e[m].abs() <= eps * tst1 {
                    break;
                }
                m = m + 1;
            }

            // If m == l, self.d[l] is an eigenvalue,
            // otherwise, iterate.
            if m > l {
                let mut iter = 0;
                loop {
                    iter = iter + 1; // (Could check iteration count here.)
                    let MAX_ITERATIONS = 100;
                    if iter > MAX_ITERATIONS {
                        eprintln!(
                            "The algorithm tql2 has not converged after {} iterations",
                            MAX_ITERATIONS
                        );
                        break;
                    }

                    // Compute implicit shift
                    let mut g = self.d[l];
                    let mut p = (self.d[l + 1] - g) / (two * self.e[l]);
                    let mut r = p.hypot(T::one());
                    if p < T::zero() {
                        r = -r;
                    }
                    self.d[l] = self.e[l] / (p + r);
                    self.d[l + 1] = self.e[l] * (p + r);
                    let dl1 = self.d[l + 1];
                    let mut h = g - self.d[l];
                    for i in (l + 2)..self.n {
                        self.d[i] = self.d[i] - h;
                    }
                    f = f + h;

                    // Implicit QL transformation.
                    p = self.d[m];
                    let mut c = T::one();
                    let mut c2 = c;
                    let mut c3 = c;
                    let el1 = self.e[l + 1];
                    let mut s = T::zero();
                    let mut s2 = T::zero();
                    for i in (l..m).rev() {
                        c3 = c2;
                        c2 = c;
                        s2 = s;
                        g = c * self.e[i];
                        h = c * p;
                        r = p.hypot(self.e[i]);
                        self.e[i + 1] = s * r;
                        s = self.e[i] / r;
                        c = p / r;
                        p = c * self.d[i] - s * g;
                        self.d[i + 1] = h + s * (c * g + s * self.d[i]);

                        // Accumulate transformation.
                        for k in 0..self.n {
                            h = self.V[(k, i + 1)];
                            self.V[(k, i + 1)] = s * self.V[(k, i)] + c * h;
                            self.V[(k, i)] = c * self.V[(k, i)] - s * h;
                        }
                    }
                    p = -s * s2 * c3 * el1 * self.e[l] / dl1;
                    self.e[l] = s * p;
                    self.d[l] = c * p;

                    // Check for convergence.
                    if self.e[l].abs() <= eps * tst1 {
                        break;
                    }
                }
            }
            self.d[l] = self.d[l] + f;
            self.e[l] = T::zero();
        }

        // Sort eigenvalues and corresponding vectors.
        for i in 0..(self.n - 1) {
            let mut k = i;
            let mut p = self.d[i];
            for j in (i + 1)..self.n {
                if self.d[j] < p {
                    k = j;
                    p = self.d[j];
                }
            }
            if k != i {
                self.d[k] = self.d[i];
                self.d[i] = p;
                for j in 0..self.n {
                    self.V.swap((j, i), (j, k));
                }
            }
        }
    }

    /// Nonsymmetric reduction to Hessenberg form.
    ///
    /// This is derived from the Algol procedures orthes and ortran,
    /// by Martin and Wilkinson, Handbook for Auto. Comp.,
    /// Vol.ii-Linear Algebra, and the corresponding
    /// Fortran subroutines in EISPACK.
    fn orthes(&mut self) {
        let low = self.low;
        let high = self.high - 1;

        for m in (low + 1)..high {
            // Scale column.
            let mut scale = T::zero();
            for i in m..=high {
                scale = scale + self.H[(i, m - 1)].abs();
            }
            if !scale.is_zero() {
                // Compute Householder transformation.
                let mut h = T::zero();
                for i in (m..=high).rev() {
                    self.ort[i] = self.H[(i, m - 1)] / scale;
                    h = h + self.ort[i] * self.ort[i];
                }
                let mut g = h.sqrt();
                if self.ort[m] > T::zero() {
                    g = -g;
                }
                h = h - self.ort[m] * g;
                self.ort[m] = self.ort[m] - g;

                // Apply Householder similarity transformation
                // H = (I-u*u'/h)*H*(I-u*u')/h)
                for j in m..self.n {
                    let mut f = T::zero();
                    for i in (m..=high).rev() {
                        f = f + self.ort[i] * self.H[(i, j)];
                    }
                    f = f / h;
                    for i in m..=high {
                        self.H[(i, j)] = self.H[(i, j)] - f * self.ort[i];
                    }
                }

                for i in 0..=high {
                    let mut f = T::zero();
                    for j in (m..=high).rev() {
                        f = f + self.ort[j] * self.H[(i, j)];
                    }
                    f = f / h;
                    for j in m..=high {
                        self.H[(i, j)] = self.H[(i, j)] - f * self.ort[j];
                    }
                }
                self.ort[m] = scale * self.ort[m];
                self.H[(m, m - 1)] = scale * g;
            }
        }

        // Accumulate transformations (Algol's ortran).
        for i in 0..self.n {
            for j in 0..self.n {
                self.V[(i, j)] = if i == j { T::one() } else { T::zero() };
            }
        }

        for m in ((low + 1)..high).rev() {
            if self.H[(m, m - 1)] != T::zero() {
                for i in (m + 1)..=high {
                    self.ort[i] = self.H[(i, m - 1)];
                }
                for j in m..=high {
                    let mut g = T::zero();
                    for i in m..=high {
                        g = g + self.ort[i] * self.V[(i, j)];
                    }
                    // Double division avoids possible underflow
                    g = (g / self.ort[m]) / self.H[(m, m - 1)];
                    for i in m..=high {
                        self.V[(i, j)] = self.V[(i, j)] + g * self.ort[i];
                    }
                }
            }
        }
    }

    /// Complex scalar division.
    fn cdiv(xr: T, xi: T, yr: T, yi: T) -> (T, T) {
        let (cdivr, cdivi) = if yr.abs() > yi.abs() {
            let r = yi / yr;
            let d = yr + r * yi;
            ((xr + r * xi) / d, (xi - r * xr) / d)
        } else {
            let r = yr / yi;
            let d = yi + r * yr;
            ((r * xr + xi) / d, (r * xi - xr) / d)
        };
        (cdivr, cdivi)
    }

    /// Nonsymmetric reduction from Hessenberg to real Schur form.
    ///
    /// This is derived from the Algol procedure hqr2,
    /// by Martin and Wilkinson, Handbook for Auto. Comp.,
    /// Vol.ii-Linear Algebra, and the corresponding
    /// Fortran subroutine in EISPACK.
    #[allow(clippy::assign_op_pattern, clippy::too_many_lines)]
    fn hqr2(&mut self) {
        let two = T::one() + T::one();

        // Initialize
        let nn = self.n;
        let mut n = nn - 1;
        let low = 0;
        let high = nn - 1;
        let eps = T::epsilon();
        let mut exshift = T::zero();
        let mut p = T::zero();
        let mut q = T::zero();
        let mut r = T::zero();
        let mut s = T::zero();
        let mut z = T::zero();
        let mut t: T;
        let mut w: T;
        let mut x: T;
        let mut y: T;

        // Store roots isolated by balanc and compute matrix norm
        let mut norm = T::zero();
        for i in 0..nn {
            if i < low || i > high {
                self.d[i] = self.H[(i, i)];
                self.e[i] = T::zero();
            }
            for j in i.saturating_sub(1)..nn {
                norm = norm + self.H[(i, j)].abs();
            }
        }

        // Outer loop over eigenvalue index
        let mut iter = 0;
        while n >= low {
            // Look for single small sub-diagonal element
            let mut l = n;
            while l > low {
                s = self.H[(l - 1, l - 1)].abs() + self.H[(l, l)].abs();
                if s.is_zero() {
                    s = norm;
                }
                if self.H[(l, l - 1)].abs() < eps * s {
                    break;
                }
                l = l - 1;
            }

            // Check for convergence
            // One root found
            if l == n {
                self.H[(n, n)] = self.H[(n, n)] + exshift;
                self.d[n] = self.H[(n, n)];
                self.e[n] = T::zero();
                if let Some(sub) = n.checked_sub(1) {
                    n = sub;
                } else {
                    break;
                }
                iter = 0;

            // Two roots found
            } else if l == n - 1 {
                w = self.H[(n, n - 1)] * self.H[(n - 1, n)];
                p = (self.H[(n - 1, n - 1)] - self.H[(n, n)]) / two;
                q = p * p + w;
                z = q.abs().sqrt();
                self.H[(n, n)] = self.H[(n, n)] + exshift;
                self.H[(n - 1, n - 1)] = self.H[(n - 1, n - 1)] + exshift;
                x = self.H[(n, n)];

                // Real pair
                if q >= T::zero() {
                    if p >= T::zero() {
                        z = p + z;
                    } else {
                        z = p - z;
                    }
                    self.d[n - 1] = x + z;
                    self.d[n] = self.d[n - 1];
                    if z != T::zero() {
                        self.d[n] = x - w / z;
                    }
                    self.e[n - 1] = T::zero();
                    self.e[n] = T::zero();
                    x = self.H[(n, n - 1)];
                    s = x.abs() + z.abs();
                    p = x / s;
                    q = z / s;
                    r = (p * p + q * q).sqrt();
                    p = p / r;
                    q = q / r;

                    // Row modification
                    for j in (n - 1)..nn {
                        z = self.H[(n - 1, j)];
                        self.H[(n - 1, j)] = q * z + p * self.H[(n, j)];
                        self.H[(n, j)] = q * self.H[(n, j)] - p * z;
                    }

                    // Column modification
                    for i in 0..=n {
                        z = self.H[(i, n - 1)];
                        self.H[(i, n - 1)] = q * z + p * self.H[(i, n)];
                        self.H[(i, n)] = q * self.H[(i, n)] - p * z;
                    }

                    // Accumulate transformations
                    for i in low..=high {
                        z = self.V[(i, n - 1)];
                        self.V[(i, n - 1)] = q * z + p * self.V[(i, n)];
                        self.V[(i, n)] = q * self.V[(i, n)] - p * z;
                    }

                // Complex pair
                } else {
                    self.d[n - 1] = x + p;
                    self.d[n] = x + p;
                    self.e[n - 1] = z;
                    self.e[n] = -z;
                }
                if let Some(sub) = n.checked_sub(2) {
                    n = sub;
                } else {
                    break;
                }
                iter = 0;

            // No convergence yet
            } else {
                // Form shift
                x = self.H[(n, n)];
                y = T::zero();
                w = T::zero();
                if l < n {
                    y = self.H[(n - 1, n - 1)];
                    w = self.H[(n, n - 1)] * self.H[(n - 1, n)];
                }

                // Wilkinson's original ad hoc shift
                if iter == 10 {
                    exshift = exshift + x;
                    for i in low..=n {
                        self.H[(i, i)] = self.H[(i, i)] - x;
                    }
                    s = self.H[(n, n - 1)].abs() + self.H[(n - 1, n - 2)].abs();
                    x = T::_075() * s;
                    y = T::_075() * s;
                    w = T::_m04375() * s * s;
                }

                // MATLAB's new ad hoc shift
                if iter == 30 {
                    s = (y - x) / two;
                    s = s * s + w;
                    if s > T::zero() {
                        s = s.sqrt();
                        if y < x {
                            s = -s;
                        }
                        s = x - w / ((y - x) / two + s);
                        for i in low..=n {
                            self.H[(i, i)] = self.H[(i, i)] - s;
                        }
                        exshift = exshift + s;
                        x = T::_0964();
                        y = T::_0964();
                        w = T::_0964();
                    }
                }

                iter = iter + 1; // (Could check iteration count here.)
                let MAX_ITERATIONS = 100;
                if iter > MAX_ITERATIONS {
                    eprintln!(
                        "The algorithm hqr2 has not converged after {} iterations",
                        MAX_ITERATIONS
                    );
                    break;
                }

                // Look for two consecutive small sub-diagonal elements
                let mut m = n - 2;
                while m >= l {
                    z = self.H[(m, m)];
                    r = x - z;
                    s = y - z;
                    p = (r * s - w) / self.H[(m + 1, m)] + self.H[(m, m + 1)];
                    q = self.H[(m + 1, m + 1)] - z - r - s;
                    r = self.H[(m + 2, m + 1)];
                    s = p.abs() + q.abs() + r.abs();
                    p = p / s;
                    q = q / s;
                    r = r / s;
                    if m == l {
                        break;
                    }
                    if self.H[(m, m - 1)].abs() * (q.abs() + r.abs())
                        < eps
                            * (p.abs()
                                * (self.H[(m - 1, m - 1)].abs()
                                    + z.abs()
                                    + self.H[(m + 1, m + 1)].abs()))
                    {
                        break;
                    }
                    m = m - 1;
                }

                for i in (m + 2)..=n {
                    self.H[(i, i - 2)] = T::zero();
                    if i > m + 2 {
                        self.H[(i, i - 3)] = T::zero();
                    }
                }

                // Double QR step involving rows l:n and columns m:n
                for k in m..n {
                    let notlast: bool = k != n - 1;
                    if k != m {
                        p = self.H[(k, k - 1)];
                        q = self.H[(k + 1, k - 1)];
                        r = if notlast {
                            self.H[(k + 2, k - 1)]
                        } else {
                            T::zero()
                        };
                        x = p.abs() + q.abs() + r.abs();
                        if x.is_zero() {
                            continue;
                        }
                        p = p / x;
                        q = q / x;
                        r = r / x;
                    }

                    s = (p * p + q * q + r * r).sqrt();
                    if p < T::zero() {
                        s = -s;
                    }
                    if s != T::zero() {
                        if k != m {
                            self.H[(k, k - 1)] = -s * x;
                        } else if l != m {
                            self.H[(k, k - 1)] = -self.H[(k, k - 1)];
                        }
                        p = p + s;
                        x = p / s;
                        y = q / s;
                        z = r / s;
                        q = q / p;
                        r = r / p;

                        // Row modification
                        for j in k..nn {
                            p = self.H[(k, j)] + q * self.H[(k + 1, j)];
                            if notlast {
                                p = p + r * self.H[(k + 2, j)];
                                self.H[(k + 2, j)] = self.H[(k + 2, j)] - p * z;
                            }
                            self.H[(k, j)] = self.H[(k, j)] - p * x;
                            self.H[(k + 1, j)] = self.H[(k + 1, j)] - p * y;
                        }

                        // Column modification
                        for i in 0..=n.min(k + 3) {
                            p = x * self.H[(i, k)] + y * self.H[(i, k + 1)];
                            if notlast {
                                p = p + z * self.H[(i, k + 2)];
                                self.H[(i, k + 2)] = self.H[(i, k + 2)] - p * r;
                            }
                            self.H[(i, k)] = self.H[(i, k)] - p;
                            self.H[(i, k + 1)] = self.H[(i, k + 1)] - p * q;
                        }

                        // Accumulate transformations
                        for i in low..=high {
                            p = x * self.V[(i, k)] + y * self.V[(i, k + 1)];
                            if notlast {
                                p = p + z * self.V[(i, k + 2)];
                                self.V[(i, k + 2)] = self.V[(i, k + 2)] - p * r;
                            }
                            self.V[(i, k)] = self.V[(i, k)] - p;
                            self.V[(i, k + 1)] = self.V[(i, k + 1)] - p * q;
                        }
                    } // (s != 0)
                } // k loop
            } // check convergence
        } // while (n >= low)

        // Backsubstitute to find vectors of upper triangular form
        if norm.is_zero() {
            return;
        }

        for n in (0..nn).rev() {
            p = self.d[n];
            q = self.e[n];

            // Real vector
            if q.is_zero() {
                let mut l = n;
                self.H[(n, n)] = T::one();
                for i in (0..n).rev() {
                    w = self.H[(i, i)] - p;
                    r = T::zero();
                    for j in l..=n {
                        r = r + self.H[(i, j)] * self.H[(j, n)];
                    }
                    if self.e[i] < T::zero() {
                        z = w;
                        s = r;
                    } else {
                        l = i;
                        if self.e[i].is_zero() {
                            if w.is_zero() {
                                self.H[(i, n)] = -r / (eps * norm);
                            } else {
                                self.H[(i, n)] = -r / w;
                            }

                        // Solve real equations
                        } else {
                            x = self.H[(i, i + 1)];
                            y = self.H[(i + 1, i)];
                            q = (self.d[i] - p) * (self.d[i] - p) + self.e[i] * self.e[i];
                            t = (x * s - z * r) / q;
                            self.H[(i, n)] = t;
                            if x.abs() > z.abs() {
                                self.H[(i + 1, n)] = (-r - w * t) / x;
                            } else {
                                self.H[(i + 1, n)] = (-s - y * t) / z;
                            }
                        }

                        // Overflow control
                        t = self.H[(i, n)].abs();
                        if (eps * t) * t > T::one() {
                            for j in i..=n {
                                self.H[(j, n)] = self.H[(j, n)] / t;
                            }
                        }
                    }
                }

            // Complex vector
            } else if q < T::zero() {
                let mut l = n - 1;

                // Last vector component imaginary so matrix is triangular
                if self.H[(n, n - 1)].abs() > self.H[(n - 1, n)].abs() {
                    self.H[(n - 1, n - 1)] = q / self.H[(n, n - 1)];
                    self.H[(n - 1, n)] = -(self.H[(n, n)] - p) / self.H[(n, n - 1)];
                } else {
                    let (cdivr, cdivi) = Self::cdiv(
                        T::zero(),
                        -self.H[(n - 1, n)],
                        self.H[(n - 1, n - 1)] - p,
                        q,
                    );
                    self.H[(n - 1, n - 1)] = cdivr;
                    self.H[(n - 1, n)] = cdivi;
                }
                self.H[(n, n - 1)] = T::zero();
                self.H[(n, n)] = T::one();
                for i in (1..n).rev() {
                    let i = i - 1; // Avoid usize underflow
                    let mut ra: T;
                    let mut sa: T;
                    let mut vr: T;
                    let vi: T;
                    ra = T::zero();
                    sa = T::zero();
                    for j in l..=n {
                        ra = ra + self.H[(i, j)] * self.H[(j, n - 1)];
                        sa = sa + self.H[(i, j)] * self.H[(j, n)];
                    }
                    w = self.H[(i, i)] - p;

                    if self.e[i] < T::zero() {
                        z = w;
                        r = ra;
                        s = sa;
                    } else {
                        l = i;
                        if self.e[i].is_zero() {
                            let (cdivr, cdivi) = Self::cdiv(-ra, -sa, w, q);
                            self.H[(i, n - 1)] = cdivr;
                            self.H[(i, n)] = cdivi;
                        } else {
                            // Solve complex equations
                            x = self.H[(i, i + 1)];
                            y = self.H[(i + 1, i)];
                            vr = (self.d[i] - p) * (self.d[i] - p) + self.e[i] * self.e[i] - q * q;
                            vi = (self.d[i] - p) * two * q;
                            if vr.is_zero() && vi.is_zero() {
                                vr = eps * norm * (w.abs() + q.abs() + x.abs() + y.abs() + z.abs());
                            }
                            let (cdivr, cdivi) = Self::cdiv(
                                x * r - z * ra + q * sa,
                                x * s - z * sa - q * ra,
                                vr,
                                vi,
                            );
                            self.H[(i, n - 1)] = cdivr;
                            self.H[(i, n)] = cdivi;
                            if x.abs() > (z.abs() + q.abs()) {
                                self.H[(i + 1, n - 1)] =
                                    (-ra - w * self.H[(i, n - 1)] + q * self.H[(i, n)]) / x;
                                self.H[(i + 1, n)] =
                                    (-sa - w * self.H[(i, n)] - q * self.H[(i, n - 1)]) / x;
                            } else {
                                let (cdivr, cdivi) = Self::cdiv(
                                    -r - y * self.H[(i, n - 1)],
                                    -s - y * self.H[(i, n)],
                                    z,
                                    q,
                                );
                                self.H[(i + 1, n - 1)] = cdivr;
                                self.H[(i + 1, n)] = cdivi;
                            }
                        }

                        // Overflow control
                        t = self.H[(i, n - 1)].abs().max(&self.H[(i, n)].abs());
                        if (eps * t) * t > T::one() {
                            for j in i..=n {
                                self.H[(j, n - 1)] = self.H[(j, n - 1)] / t;
                                self.H[(j, n)] = self.H[(j, n)] / t;
                            }
                        }
                    }
                }
            }
        }

        // Vectors of isolated roots
        for i in 0..nn {
            if i < low || i > high {
                for j in i..nn {
                    self.V[(i, j)] = self.H[(i, j)];
                }
            }
        }

        // Back transformation to get eigenvectors of original matrix
        for j in (low..nn).rev() {
            for i in low..=high {
                z = T::zero();
                for k in low..=j.min(high) {
                    z = z + self.V[(i, k)] * self.H[(k, j)];
                }
                self.V[(i, j)] = z;
            }
        }
    }

    /// Create an `EigenvalueDecomposition` structure with all elements set to zero.
    fn new_empty(n: usize) -> Self {
        EigenvalueDecomposition {
            n,
            issymmetric: true,
            d: Array1::from_elem(n, T::zero()),
            e: Array1::from_elem(n, T::zero()),
            V: Array2::from_elem((n, n), T::zero()),
            H: Array2::<T>::from_elem((1, 1), T::zero()),
            ort: Array1::<T>::from_elem(0, T::zero()),
            scale: Array1::<Scale<T>>::from_elem(0, Scale::Index(0)),
            low: 0,
            high: n,
        }
    }

    /// Check if the square matrix `A` of size `n` is symmetric.
    fn is_symmetric_matrix(A: &Array2<T>, n: usize) -> bool {
        for i in 0..n {
            for j in 0..n {
                if A[(i, j)] != A[(j, i)] {
                    return false;
                }
            }
        }
        true
    }

    /// Check for symmetry, then construct the eigenvalue decomposition
    /// structure to access D and V.
    ///
    /// # Arguments
    ///
    /// * `Arg` - Square matrix
    ///
    /// # Example
    /// ```text
    /// use ndarray::arr2;
    /// let A = arr2(&[[4., 1., 1.], [1., 2., 3.], [1., 3., 6.]]);
    /// let eig = EigenvalueDecomposition::new(&A);
    /// ```
    pub(crate) fn new(Arg: &Array2<T>) -> Self {
        let A: Array2<T> = Arg.clone();
        let n = Arg.ncols();

        let mut evd = Self::new_empty(n);

        evd.issymmetric = Self::is_symmetric_matrix(&A, n);

        if evd.issymmetric {
            evd.V = A;

            // Tridiagonalize.
            evd.tred2();

            // Diagonalize.
            evd.tql2();
        } else {
            evd.ort = Array1::<T>::from_elem(n, T::zero());
            evd.scale = Array1::<Scale<T>>::from_elem(n, Scale::Index(0));
            evd.H = A;

            // Balance
            evd.balanc();

            // Reduce to Hessenberg form.
            evd.orthes();

            // Reduce Hessenberg to real Schur form.
            evd.hqr2();

            // Back transforming eigenvectors transformed by balanc.
            evd.balbak();
        }
        evd
    }

    /// Return the eigenvector matrix
    pub(crate) fn get_V(&self) -> Array2<T> {
        self.V.clone()
    }

    /// Return the real parts of the eigenvalues: real(diag(D))
    pub(crate) fn get_real_eigenvalues(&self) -> Array1<T> {
        self.d.clone()
    }

    /// Return the imaginary parts of the eigenvalues: imag(diag(D))
    pub(crate) fn get_imag_eigenvalues(&self) -> Array1<T> {
        self.e.clone()
    }

    /// Return the complex eigenvalues: diag(D)
    pub(crate) fn get_eigenvalues(&self) -> Array1<(T, T)> {
        ndarray::Zip::from(&self.d)
            .and(&self.e)
            .map_collect(|&x, &y| (x, y))
    }

    /// Return the block diagonal eigenvalue matrix: D
    pub(crate) fn get_D(&self) -> Array2<T> {
        let mut D = Array2::<T>::from_elem((self.n, self.n), T::zero());
        for i in 0..self.n {
            for j in 0..self.n {
                D[(i, j)] = T::zero();
            }
            D[(i, i)] = self.d[i];
            if self.e[i] > T::zero() {
                D[(i, i + 1)] = self.e[i];
            } else if self.e[i] < T::zero() {
                D[(i, i - 1)] = self.e[i];
            }
        }
        D
    }
}

/// Trait that defines the constants used in the Eigen solver.
#[allow(clippy::module_name_repetitions)]
pub trait EigenConst {
    /// 0.75 constant
    fn _075() -> Self;
    /// -0.4375 constant
    fn _m04375() -> Self;
    /// 0.964 constant
    fn _0964() -> Self;
    /// 0.95 constant
    fn _095() -> Self;
}

macro_rules! impl_eigen_const {
    ($t:ty) => {
        impl EigenConst for $t {
            fn _075() -> Self {
                0.75
            }
            fn _m04375() -> Self {
                -0.4375
            }
            fn _0964() -> Self {
                0.964
            }
            fn _095() -> Self {
                0.95
            }
        }
    };
}

impl_eigen_const!(f32);
impl_eigen_const!(f64);

#[cfg(test)]
mod tests {
    use super::*;
    use crate::matrix::MatrixMul;
    use ndarray::{arr2, Zip};

    fn check_eq(X: &Array2<f64>, Y: &Array2<f64>) -> bool {
        let eps = f64::EPSILON; // 2.0_f64.powf(-52.0);
        Zip::from(X)
            .and(Y)
            .all(|&x, &y| (x - y).abs() < 1000.0 * eps)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn balance4x4() {
        let mut evd = EigenvalueDecomposition::new_empty(4);
        evd.issymmetric = false;
        evd.scale = Array1::from_elem(4, Scale::Index(0));
        evd.H = arr2(&[
            [11., 0., 13., 14.],
            [21., 22., 23., 24.],
            [0., 0., 33., 0.],
            [41., 0., 43., 44.],
        ]);
        evd.balanc();
        // [22., 21., 24., 23.],
        // [ 0., 11., 14., 13.],
        // [ 0., 41., 44., 43.],
        // [ 0.,  0.,  0., 33.],
        assert_eq!(Scale::Index(2), evd.scale[3]);
        assert_eq!(Scale::Index(1), evd.scale[0]);
        for i in 1..4 {
            assert_eq!(0., evd.H[(i, 0)]);
        }
        for j in 0..3 {
            assert_eq!(0., evd.H[(3, j)]);
        }
    }

    #[test]
    fn single_value_matrix() {
        let A = arr2(&[[17.]]);
        let eig = EigenvalueDecomposition::new(&A);
        let d = eig.get_eigenvalues();

        assert_relative_eq!(17., d[0].0);
        assert_relative_eq!(0., d[0].1);
    }

    #[test]
    fn symmetric_matrix() {
        let A = arr2(&[[4., 1., 1.], [1., 2., 3.], [1., 3., 6.]]);
        let eig = EigenvalueDecomposition::new(&A);
        let D = eig.get_D();
        let V = eig.get_V();
        assert!(check_eq(&A.mmul(&V), &V.mmul(&D)));
    }

    #[test]
    fn non_symmetric_matrix() {
        let A = arr2(&[
            [0., 1., 0., 0.],
            [1., 0., 2.0e-7, 0.],
            [0., -2.0e-7, 0., 1.],
            [0., 0., 1., 0.],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let D = eig.get_D();
        let V = eig.get_V();
        assert!(check_eq(&A.mmul(&V), &V.mmul(&D)));
    }

    #[test]
    fn almost_calculated_matrix() {
        let A = arr2(&[
            [1., 0., 0., 0.],
            [0., 2., 0., 0.],
            [0., 0., 0., -1.],
            [0., 0., 1., 0.],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let D = eig.get_D();
        let V = eig.get_V();
        assert!(check_eq(&A.mmul(&V), &V.mmul(&D)));
    }

    #[test]
    fn non_symmetric_2x2_matrix() {
        let A = arr2(&[[-2., 0.], [3., -7.]]);
        let eig = EigenvalueDecomposition::new(&A);
        let D = eig.get_D();
        let V = eig.get_V();
        assert!(check_eq(&A.mmul(&V), &V.mmul(&D)));
        assert_eq!(D, arr2(&[[-7., 0.], [0., -2.]]));
        assert_eq!(V, arr2(&[[0., 1.], [1., 0.6]]));
    }

    #[test]
    fn bad_eigenvalues() {
        let bA = arr2(&[
            [0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 1.],
            [0., 0., 0., 1., 0.],
            [1., 1., 0., 0., 1.],
            [1., 0., 1., 0., 1.],
        ]);
        let _bEig = EigenvalueDecomposition::new(&bA);
        println!("\nTesting Eigenvalue; If this hangs, we've failed\n");
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn linear_system_poles() {
        let A = arr2(&[
            [0., 0., 0., 1., 0., 0.],
            [0., 0., 0., 0., 1., 0.],
            [0., 0., 0., 0., 0., 1.],
            [-2., 1., 0., 0., 0., 0.],
            [1., -2., 1., 0., 0., 0.],
            [0., 1., -2., 0., 0., 0.],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let d = eig.get_real_eigenvalues();
        let e = eig.get_imag_eigenvalues();
        assert!(d.iter().all(|re| relative_eq!(*re, 0.)));
        for i in e.as_slice().unwrap().chunks(2) {
            assert_eq!(i[0], -i[1]);
        }
        let V = eig.get_V();
        assert_relative_eq!(-(2.0.sqrt()), V[(1, 0)] / V[(0, 0)], epsilon = 1.0e-15);
        assert_relative_eq!(-(2.0.sqrt()), V[(1, 0)] / V[(2, 0)], epsilon = 1.0e-15);
        assert_relative_eq!(1., V[(0, 0)] / V[(0, 0)]);
    }

    #[test]
    #[allow(clippy::excessive_precision)]
    fn no_endless_loop() {
        // The following matrix eigenvalue decomposition is unstable
        // if the numeric type is f32, in case of f64 the decomposition
        // returns the correct values.
        // This test is to check that the algorithm return (even if the result
        // is wrong) and it is not stuck inside an endless loop.
        let A = arr2(&[
            [0.0_f32, 1., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.],
            [
                -6.249_999_97e+10,
                -1.263_125e+12,
                -6.828_475_29e+12,
                -2.038_578_54e+12,
                -7.359_804_21e+11,
                -7.862_141_75e+10,
                -1.056_018_12e+10,
                -154_605_568.,
                -4_472_637.,
                -33_713.007_8,
                -440.200_012,
            ],
        ]);
        let eig = EigenvalueDecomposition::new(&A);
        let lambda = eig.get_eigenvalues();
        assert_eq!(lambda[0], (-380.14774, 0.));
    }
}
