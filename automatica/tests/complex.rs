#[macro_use]
extern crate approx;

use automatica::{damp, pulse, Complex};

/// TC6.1
#[test]
fn damping_of_zero() {
    let zero = Complex::new(0., 0.);
    assert_relative_eq!(0., pulse(zero));
    assert_relative_eq!(-1., damp(zero));
}
