use automatica::Ss;

#[allow(clippy::many_single_char_names)]
fn main() {
    // Three bodies with equal mass joined by four equal springs fixed at each side
    // |
    // |--/\/\/\/--o--/\/\/\/--o--/\/\/\/--o--/\/\/\/--|
    // |     k     m     k     m     k     m     k
    //
    // Mass (m), spring (k)
    // external force (u)
    // position (x1, x2, x3), speed (x4, x5, x6)
    // position (y1=x1, y2=x2, y3=x3)
    let m = 1.;
    let k = 1.;
    let c = k / m;

    let a: Vec<f64> = [
        [0., 0., 0., 1., 0., 0.],              // xdot1
        [0., 0., 0., 0., 1., 0.],              // xdot2
        [0., 0., 0., 0., 0., 1.],              // xdot3
        [-2. * c, 1. * c, 0., 0., 0., 0.],     // xdot4
        [1. * c, -2. * c, 1. * c, 0., 0., 0.], // xdot5
        [0., 1. * c, -2. * c, 0., 0., 0.],     // xdot6
    ]
    .into_iter()
    .flatten()
    .collect();
    let b = [0., 0., 0., 0., 0., 0.];
    let c = [
        1., 0., 0., 0., 0., 0., // y1.
        0., 1., 0., 0., 0., 0., // y2
        0., 0., 1., 0., 0., 0., // y3
    ];
    let d = [0., 0., 0.];

    let sys = Ss::new_from_slice(6, 1, 3, &a, &b, &c, &d);
    println!("{}", &sys);

    let poles = sys.poles();
    println!("\nPoles:");
    println!("\t{:.2}, {:.2}", poles[0], poles[1]);
    println!("\t{:.2}, {:.2}", poles[2], poles[3]);
    println!("\t{:.2}, {:.2}", poles[4], poles[5]);

    let v = sys.eigenvectors();
    println!("\nEigenvectors:");
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][0], v[1][0], v[2][0], v[3][0], v[4][0], v[5][0],
    );
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][1], v[1][1], v[2][1], v[3][1], v[4][1], v[5][1],
    );
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][2], v[1][2], v[2][2], v[3][2], v[4][2], v[5][2],
    );
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][3], v[1][3], v[2][3], v[3][3], v[4][3], v[5][3],
    );
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][4], v[1][4], v[2][4], v[3][4], v[4][4], v[5][4],
    );
    println!(
        "\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t{:.3}\t",
        v[0][5], v[1][5], v[2][5], v[3][5], v[4][5], v[5][5],
    );
}
